package com.shop.shoppingApp

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context
import com.google.firebase.FirebaseApp
import com.shop.shoppingApp.util.CartCount

class ShoppingApp : Application() {

    companion object{
       @SuppressLint("StaticFieldLeak")
       lateinit var appCtx:Context
       lateinit var cartCount: CartCount
       var isFromOrder= false
       var txnCancel= false
       var transactionId= ""
    }
    override fun onCreate() {
        super.onCreate()
        appCtx=applicationContext
    }
}