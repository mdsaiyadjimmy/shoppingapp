package com.shop.shoppingApp.api

import com.shop.shoppingApp.util.NetworkUtil
import com.shop.shoppingApp.util.log
import com.shop.shoppingApp.util.toast
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

fun <T : Any> callApiG(call: Call<T>, fxn: (res: Any,IsSuccess:Boolean) -> Unit) {
    if (NetworkUtil.isConnected()) {
       // showDialog()
        call.enqueue(object : Callback<T> {
            override fun onResponse(
                call: Call<T>,
                response: Response<T>
            ) {
                val res = response.body()
                log("", "response......." + res?.toString())
                if (response.isSuccessful && res != null) {
                    log("", "SUCCESS")
                    fxn(res,true)
                } else {
                    log("", "NULL")
                    toast(response.code().toString())
                    fxn(call,false)
                }
            }
            override fun onFailure(call: Call<T>, t: Throwable) {
                log("TAG", t.message)
                fxn(call,false)
            }
        })
    }
}
