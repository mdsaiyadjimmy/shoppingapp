package com.shop.shoppingApp.api

import com.houzzcartuk.model.placeModel.ResponsePlacesList
import com.shop.shoppingApp.model.cart.AddToCartResponse
import com.shop.shoppingApp.model.cart.CartResponse
import com.shop.shoppingApp.model.cart.CartTotalResponse
import com.shop.shoppingApp.model.category.CategoryResponse
import com.shop.shoppingApp.model.commonModel.CommonResponse
import com.shop.shoppingApp.model.createOrder.CreateOrderResponse
import com.shop.shoppingApp.model.loginModel.LoginSignupResponse
import com.shop.shoppingApp.model.notification.NotificationResponse
import com.shop.shoppingApp.model.orders.OrderDetailResponse
import com.shop.shoppingApp.model.orders.OrdersResponse
import com.shop.shoppingApp.model.productHomepage.HomepageResponse
import com.shop.shoppingApp.model.products.ProductResponse
import com.shop.shoppingApp.model.settings.SettingsResponse
import com.shop.shoppingApp.model.wallet.WalletResponse
import com.shop.shoppingApp.util.SharedPref
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

interface ApiServices {

    //************otp**************************

    @FormUrlEncoded
    @POST("api/login-verify.php")
    fun login(
        @Field("mobile") mobile: String,
        @Field("otp_code") otp: String,
        @Field("firebase_token") firebase_token: String
    ): Call<LoginSignupResponse>

    @FormUrlEncoded
    @POST("api/login-request.php")
    fun sendOtp(
        @Field("mobile") mobile: String
    ): Call<CommonResponse>


    // https://maps.googleapis.com/maps/api/place/autocomplete/json?input=110094&components=country:IN&key=AIzaSyDC8W5v288i7Mjy6PNH4mAjRH6vsBQmsm0
    @GET("https://maps.googleapis.com/maps/api/place/autocomplete/json")
    fun getPlacesList(
        @Query("input") input: String,
        @Query("location") location: String? = null,
        @Query("radius") radius: String? = null,
        @Query("sensor") sensor: Boolean? = true,
        @Query("language") language: String? = "en",
        @Query("key") key: String
    ): Call<ResponsePlacesList>


    @GET("api/dashboard.php")
    fun getProductHomepage(
        @Header("accesstoken") token: String,
    ): Call<HomepageResponse>

    @GET("api/get-categories.php")
    fun getcat(
        @Header("accesstoken") token: String,
    ): Call<CategoryResponse>

    @FormUrlEncoded
    @POST("api/check-pincode.php")
    fun pinCheck(
        @Header("accesstoken") token: String,
        @Field("postcode") pin: String,
    ): Call<CommonResponse>

    @FormUrlEncoded
    @POST("api/get-products.php")
    fun getProduct(
        @Header("accesstoken") token: String,
        @Field("category") category: String = "",
        @Field("page") page: String
    ): Call<ProductResponse>

    @FormUrlEncoded
    @POST("api/search-products.php")
    fun searchProduct(
        @Header("accesstoken") token: String,
        @Field("name") key: String
    ): Call<ProductResponse>

    @FormUrlEncoded
    @POST("api/add-to-cart.php")
    fun addToCart(
        @Header("accesstoken") token: String,
        @Field("product_id") product_id: String,
        @Field("variant_id") variant_id: String,
        @Field("qty") qty: String
    ): Call<AddToCartResponse>

    @GET("api/get-cart.php")
    fun getCart(
        @Header("accesstoken") token: String
    ): Call<CartResponse>

    @FormUrlEncoded
    @POST("api/getCartTotal.php")
    fun getCartTotal(
        @Field("coupon") coupon: String,
        @Field("used_wallet") used_wallet: String,
        @Header("accesstoken") token: String = SharedPref.getUSERTOKEN(),
    ): Call<CartTotalResponse>

    @FormUrlEncoded
    @POST("api/create-order.php")
    fun createOrder(
        @Field("coupon") coupon: String,
        @Field("used_wallet") used_wallet: String,
     //   @Field("payment_id") payment_id: String,
        @Field("pay_mode") pay_mode: String,
//        @Field("order_no") orderNo: String,
        @Header("accesstoken") token: String = SharedPref.getUSERTOKEN(),
    ): Call<CreateOrderResponse>  @FormUrlEncoded
    @POST("api/update-order-payment.php")
//    order_id, payment_id
    fun updatePayment(
        @Field("payment_id") payment_id: String,
        @Field("order_id") orderNo: String,
        @Header("accesstoken") token: String = SharedPref.getUSERTOKEN(),
    ): Call<CommonResponse>


    @GET("api/get-orders.php")
    fun getOrders(
        @Header("accesstoken") token: String
    ): Call<OrdersResponse>

    @GET("api/get-notification.php")
    fun getNotif(
        @Header("accesstoken") token: String
    ): Call<NotificationResponse>

    @FormUrlEncoded
    @POST("api/get-order-details.php")
    fun getOrderDetails(
        @Field("id") pay_mode: String,
        @Header("accesstoken") token: String = SharedPref.getUSERTOKEN(),
    ): Call<OrderDetailResponse>

    @FormUrlEncoded
    @POST("api/cancel-order.php")
    fun cancelOrder(
        @Field("id") pay_mode: String,
        @Header("accesstoken") token: String = SharedPref.getUSERTOKEN(),
    ): Call<CommonResponse>

    @GET("api/get-wallet.php")
    fun getWallet(
        @Header("accesstoken") token: String
    ): Call<WalletResponse>

    @GET("api/get-settings.php")
    fun getSettings(
        @Header("accesstoken") token: String = SharedPref.getUSERTOKEN(),
    ): Call<SettingsResponse>

    @FormUrlEncoded
    @POST("designer/address/add")
    fun addAddress(
        @Field("designerId") user_id: String,
        @Field("name") name: String,
        @Field("mobile") mobile: String,
        @Field("city") city: String,
        @Field("address") address: String,
        @Field("address2") address2: String,
        @Field("state") state: String,
        @Field("landmark") landmark: String,
        @Field("postalcode") pincode: String,
        @Field("addressType") type: String
    ): Call<CommonResponse>

    @FormUrlEncoded
    @POST("designer/address/edit")
    fun updateAddress(
        @Field("designerId") user_id: String,
        @Field("_id") _id: String,
        @Field("name") name: String,
        @Field("mobile") mobile: String,
        @Field("city") city: String,
        @Field("address") address: String,
        @Field("address2") address2: String,
        @Field("state") state: String,
        @Field("landmark") landmark: String,
        @Field("postalcode") pincode: String,
        @Field("addressType") type: String
    ): Call<CommonResponse>


    @Multipart
    @POST("api/profile-update.php")
    fun updateProfile(
        @Header("accesstoken") user_id: String,
        @Part("name") name: RequestBody,
        @Part("email") email: RequestBody,
        @Part("city") city: RequestBody,
        @Part("flat_no") flat_no: RequestBody,
        @Part("address") address: RequestBody,
        @Part("postcode") pincode: RequestBody,
        @Part file: MultipartBody.Part?
    ): Call<LoginSignupResponse>

    @GET("api/get-profile.php")
    fun getProfile(
        @Header("accesstoken") user_id: String
    ): Call<LoginSignupResponse>

}

