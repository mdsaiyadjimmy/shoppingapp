package com.shop.shoppingApp.fcm


import android.content.Intent
import android.os.Build
import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.shop.shoppingApp.main.activity.MainActivity
import com.shop.shoppingApp.util.SharedPref
import com.shop.shoppingApp.util.log
import org.json.JSONException


class MyFirebaseMessagingService : FirebaseMessagingService() {



    override fun onNewToken(s: String) {
        super.onNewToken(s)
        log( msg =  "NEW_TOKEN$s")
        if (s.isNullOrEmpty()){

            SharedPref.setTOKEN(s)}
    }



    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        log( msg = "TAGG " + remoteMessage?.data.toString())

        if (remoteMessage?.data!!.isNotEmpty()) {
            log( msg =  "TAGG " + remoteMessage.data.toString())
            try {
                sendNotification(remoteMessage?.data)
            } catch (e: Exception) {
                Log.e("TAG", "Exception: " + e.message)
            }
        }
    }

    private fun sendNotification(json: MutableMap<String, String>) {

        // var data: JSONObject? = null
        try {
            //data = json.getJSONObject()
            //parsing json data
            val title = json["title"].toString()
            val message = json["message"].toString()

            //creating MyNotificationManager object
            val myNotificationManager = MyNotificationManager(applicationContext)
            val intent = Intent(this, MainActivity::class.java)
            intent.putExtra("order",false)

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                myNotificationManager.showNotificationOreo(title, message, intent)
            } else {
                myNotificationManager.showNotification(title, message, intent)
            }

        } catch (e: JSONException) {
            e.printStackTrace()
        }

    }

}

