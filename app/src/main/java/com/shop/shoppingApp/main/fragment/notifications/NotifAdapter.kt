package com.shop.shoppingApp.main.fragment.notifications


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.shop.shoppingApp.R
import com.shop.shoppingApp.model.notification.NotifItem
import com.shop.shoppingApp.model.wallet.WalletItem
import com.shop.shoppingApp.util.*
import kotlinx.android.synthetic.main.item_notif.view.*
import kotlinx.android.synthetic.main.item_wallet.view.*
import kotlinx.android.synthetic.main.item_wallet.view.item_wallet_amnt
import kotlinx.android.synthetic.main.item_wallet.view.ll_item_wallet_comment

class NotifAdapter(val context: Context, var mainlist: MutableList<NotifItem>) :
    RecyclerView.Adapter<NotifAdapter.MyViewHolder>() {


    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): MyViewHolder {
        return MyViewHolder(
            LayoutInflater.from(p0.context).inflate(
                R.layout.item_notif,
                p0,
                false
            )
        )
    }

    override fun getItemCount() = mainlist.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.itemView.apply {
            mainlist[position].run {
                item_desc.text = description.toString().capsFirstWord()
                item_title.text = title?.toString()
                item_img.setImg(img)
//                setOnClickListener {
//                    if (ll_item_wallet_comment.isVisible) ll_item_wallet_comment.hide()
//                    else ll_item_wallet_comment.show()
//                }
//        item_wallet_view.setOnClickListener {
//            viewImagesAPi(
//                RetrofitClient.getClient().getExpenseImgs(
//                    SharedPref(context).getACCESSTOKEN().toString(),
//                    id.toString()
//                )
//            )
//        }

            }
        }

    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)


}