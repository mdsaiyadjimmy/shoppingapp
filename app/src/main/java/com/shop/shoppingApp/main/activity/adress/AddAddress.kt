package com.shop.shoppingApp.main.activity.adress


import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.textfield.TextInputEditText
import com.google.gson.Gson
import com.piashsarker.www.easy_utils_lib.Utils
import com.shop.shoppingApp.R
import com.shop.shoppingApp.api.RetrofitClient
import com.shop.shoppingApp.main.BaseActivtiy
import com.shop.shoppingApp.model.addressModel.DataItem
import com.shop.shoppingApp.model.commonModel.CommonResponse
import com.shop.shoppingApp.util.*
import kotlinx.android.synthetic.main.actionbar_primary.*
import kotlinx.android.synthetic.main.activity_address.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AddAddress : BaseActivtiy() {
    var isEdit = false
    var data = DataItem()
    var lat = ""
    var lng = ""
    private val requestCode = 11
    var loading = true
    lateinit var layoutManager: LinearLayoutManager
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        whiteStatusBar()
        setContentView(R.layout.activity_address)

        action_bar_title.text = "Select addresss"
        isEdit = intent.getBooleanExtra("isEdit", false)
        if (isEdit) {
            data = Gson().fromJson(intent.getStringExtra("data"), DataItem::class.java)
            setEditUserDetails()
        }

        setUserDetails()

        back.setOnClickListener {
            finish()
        }
        next.visibility = View.GONE
        add_address_submit.setOnClickListener {
            checkredntialSignup()
        }

        add_new_address_map_my_loc.setOnClickListener {
            if (checkLocPermission()) {
                val intent = Intent(c, MyNewMapActivity::class.java)
                intent.putExtra("type", "1")
                startActivityForResult(intent, requestCode)
            }
        }
    }


    private fun checkredntialSignup() {
        var type = "home"
        if (!add_address_home.isChecked) {
            type = "office"
        }
        val name = add_address_name.text.toString()
        val lname = add_address_lname.text.toString()
        val mob = add_address_mobile.text.toString()
        val semail = add_address_email.text.toString()
        val house_no = add_address_house_no.text.toString()
        val city = add_address_city.text.toString()
        val pin = add_address_pin.text.toString()
        val street = add_address_street.text.toString()
        val landmark = add_address_landmark.text.toString()
        val state = add_address_state.text.toString()

        if (!check1(add_address_name)) {

        } else if (mob.length < 10) {
            add_address_mobile.error = "Invalid Contact No."
            add_address_mobile.requestFocus()
        } else if (!check1(add_address_house_no)) {

        } else if (!Utils.isValidEmail(semail)) {
            add_address_email.error = "Field Required"
            add_address_email.requestFocus()
        } else if (!check1(add_address_city)) {

        } else if (!check1(add_address_pin)) {

        } else if (!check1(add_address_street)) {

        } else if (!check1(add_address_state)) {

        } else {
            if (NetworkUtil.isConnected()) {
                if (isEdit) {
                    callApi(
                        RetrofitClient().updateAddress(
                            SharedPref.getUSER_ID().toString(),
                            "",//data.id.toString(),
                            name,
                            mob,
                            city,
                            house_no,
                            street,
                            state,
                            landmark,
                            pin, type
                        )
                    ) {

                    }
                } else {
                    callApi(
                        RetrofitClient().addAddress(
                            SharedPref.getUSER_ID().toString(),
                            name,
                            mob,
                            city,
                            house_no,
                            street,
                            state,
                            landmark,
                            pin, type
                        )
                    ) {

                    }
                }
            }
        }
    }

    private fun setUserDetails() {
        add_address_name.setText(SharedPref.getUSER_NAME())
        add_address_email.setText(SharedPref.getUSER_EMAIL())
        add_address_mobile.setText(SharedPref.getUSER_MOB())
        add_address_house_no.setText(SharedPref.getADDRESS_ADDRESS())
        add_address_landmark.setText(SharedPref.getADDRESS_LANDMARK())
        add_address_city.setText(SharedPref.getADDRESS_CITY())
        add_address_street.setText(SharedPref.getADDRESS_ADDRESS())
        add_address_state.setText(SharedPref.getADDRESS_STATE())
        add_address_house_no.setText(SharedPref.getADDRESS_HOUSE())
        add_address_pin.setText(SharedPref.getADDRESS_ZIPCODE())

    }

    private fun setEditUserDetails() {
//        add_address_name.setText(data.name)
//        add_address_email.setText(data.email)
//        add_address_mobile.setText(data.mobile)
//        add_address_house_no.setText(data.address)
//        add_address_landmark.setText(data.landmark)
//        add_address_city.setText(data.city)
//        add_address_street.setText(data.address2)
//        add_address_state.setText(data.state)
//        add_address_pin.setText(data.postalcode)
//        val type = data.addressType
//        if (type == "home") add_address_home.isChecked = true
//        else add_address_office.isChecked = true

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == requestCode && data != null) {
            val sarea = data.getStringExtra("area")
            val myCity = data.getStringExtra("city")
            val mypin = data.getStringExtra("pin")
            val sLat = data.getStringExtra("lat")
            val sLng = data.getStringExtra("lng")
            val state = data.getStringExtra("state")
            add_address_city.setText(myCity)
            add_address_pin.setText(mypin)
            add_address_street.setText(sarea)
            add_address_state.setText(state)
            lat = sLat.toString()
            lng = sLng.toString()
            Log.d("TAG", "$sLat  $sLng")
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == 100) {
            Log.d("TAG", "permission.. ${permissions[1]}")
            Log.d("TAG", "permission grant.. ${grantResults[1]}")
            if (grantResults.size > 1 && grantResults[0] > -1) {
                Log.d("TAG", "2")
                val intent = Intent(c, MyNewMapActivity::class.java)
                intent.putExtra("type", "1")
                startActivityForResult(intent, requestCode)
            }

        } else {
            Log.d("TAG", "permission.. $permissions")
        }
    }

    private fun check1(mView: TextInputEditText?): Boolean {

        return when {
            mView?.text.toString().isEmpty() -> {
                mView?.error = "Field Required"
                mView?.requestFocus()
                false
            }
            else -> true
        }

    }


}
