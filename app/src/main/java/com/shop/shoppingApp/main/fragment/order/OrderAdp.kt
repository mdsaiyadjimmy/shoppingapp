package com.shop.shoppingApp.main.fragment.order


import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.PopupMenu
import androidx.core.view.get
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.shop.shoppingApp.R
import com.shop.shoppingApp.ShoppingApp.Companion.appCtx
import com.shop.shoppingApp.main.activity.order.OrderDetails
import com.shop.shoppingApp.model.cart.CartDataItem
import com.shop.shoppingApp.model.cart.CartItem
import com.shop.shoppingApp.model.orders.OrderItem
import com.shop.shoppingApp.model.orders.OrderItems
import com.shop.shoppingApp.util.*
import kotlinx.android.synthetic.main.item_order.view.*

class OrderAdp(var mainlist: MutableList<OrderItems>, var fxn: (pos: Int) -> Unit) :
    RecyclerView.Adapter<OrderAdp.MyViewHolder>() {
    lateinit var context: Context

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): MyViewHolder {
        context = p0.context
        return MyViewHolder(LayoutInflater.from(p0.context).inflate(R.layout.item_order, p0, false))
    }

    override fun getItemCount(): Int = mainlist.size


    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.itemView.apply {
            mainlist[position].run {
//                if (!productId.isNullOrEmpty()) {
//                    img.setImg(productId[0].image)
//                }
                item_order_service_name.text = flatNo.toString().capsWord()
                item_order_total_price.text = totalAmt.setAmount()
                item_order_service_id.text = "Booking ID: #$id"
                item_order_user_time.text = "Order Placed On :$orderDate"//?.let { getdate(it) }
                try {
                    var data = Gson().fromJson(orderItems, CartItem::class.java)
                    log(msg = data.toString())
                } catch (e: Exception) {
                }
                item_order_status.apply {

                    when (status) {
                        "pending" -> {
                            text = "Order Pending"
                            setTextClr(R.color.brown)
                            setBgClr(R.color.brown_light)
                        }
                        "accepted" -> {
                            text = "Order Accepted"
                            setTextClr(R.color.blue)
                            setBgClr(R.color.blue_light)
                        }
                        "delivered" -> {
                            text = "Order Delivered"
                            setTextClr(R.color.green)
                            setBgClr(R.color.green_light)
                        }
                        "cancelled" -> {
                            text = "Order Cancelled"
                            setTextClr(R.color.red)
                            setBgClr(R.color.pink_light)
                        }

                    }
                }
                setOnClickListener {
                    context.startActivity(
                        Intent(context, OrderDetails::class.java).putExtra(
                            "data",
                            Gson().toJson(mainlist[position])
                        )
                    )
                }
                item_order_menu.setOnClickListener {
                    optionMenu(holder.itemView, position)
                }
            }
        }
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    private fun optionMenu(view: View, position: Int) {

        val popup = PopupMenu(appCtx, view.item_order_menu)
        popup.menuInflater.inflate(R.menu.order_menu, popup.menu)
        popup.menu[0].isVisible = mainlist[position].status != "cancelled"
        popup.setOnMenuItemClickListener { item ->

            when (item.itemId) {
                R.id.cancel -> {
                    fxn(position)
                }
                R.id.detail -> {
                    context.startActivity(
                        Intent(context, OrderDetails::class.java).putExtra(
                            "data",
                            Gson().toJson(mainlist[position])
                        )
                    )
                }
            }

            true
        }

        popup.show()
    }

}