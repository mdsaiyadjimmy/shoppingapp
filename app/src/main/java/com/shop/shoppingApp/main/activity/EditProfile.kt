package com.shop.shoppingApp.main.activity

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import com.github.dhaval2404.imagepicker.ImagePicker
import com.shop.shoppingApp.R
import com.shop.shoppingApp.api.RetrofitClient
import com.shop.shoppingApp.main.BaseActivtiy
import com.shop.shoppingApp.model.commonModel.CommonResponse
import com.shop.shoppingApp.model.loginModel.LoginSignupResponse
import com.shop.shoppingApp.util.*
import kotlinx.android.synthetic.main.actionbar_primary.*
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_signup.*

class EditProfile : BaseActivtiy() {
    var sfname = ""
    var scity = ""
    var semail = ""
    var spinCode = ""
    var sflate = ""
    var saddress = ""
    var profileUri: Uri? = null
    var profileUrl = "null"
    override fun onResume() {
        super.onResume()
        dismissDialog()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        whiteStatusBar()

        SharedPref.run {
            getUSER_NAME()?.let { signup_full_name.setText(it) }
            getUSER_EMAIL()?.let { signup_email.setText(it) }
            getADDRESS_HOUSE()?.let { signup_flat.setText(it) }
            getADDRESS_ZIPCODE()?.let { signup_pin.setText(it) }
            getADDRESS_CITY()?.let { signup_city.setText(it) }
            getADDRESS_ADDRESS()?.let { signup_address.setText(it) }
            profile_pic.setImgProfile(getUSER_PIC())
            ll_number.hide()
            ll_otp.hide()
            ll_logo.hide()
            ll_signup.show()
            if (intent.getBooleanExtra("isEdit", false)) {
                profile_pic.hide()
                action_bar_title.text = "Edit Address"
            } else action_bar_title.text = "Edit Profile"
            ll_signup.hideSoftKeyboard()
        }
        signup_create_account.text = "Submit"
        profile_pic.setOnClickListener { takePicture() }
        signup_create_account.setOnClickListener { checkRegcredntial() }
        back.setOnClickListener { finish() }
    }

    private fun checkRegcredntial() {
        sfname = signup_full_name.text.toString()
        semail = signup_email.text.toString()
        sflate = signup_flat.text.toString()
        scity = signup_city.text.toString()
        saddress = signup_address.text.toString()
        spinCode = signup_pin.text.toString()
        if (!intent.getBooleanExtra("isEdit", false)) {
            if (profileUri == null && profileUrl == "null") toast("Select Profile Image")
        } else if (signup_full_name.check()) return
        else if (signup_flat.check(3)) return
        else if (signup_city.check(5)) return
        else if (signup_address.check(10)) return
        else if (signup_pin.check(6)) return
        else if (!semail.isValidEmail()) {
            signup_email.error = "Invalid Email"
            signup_email.requestFocus()
        } else {

//            recmsg()
            callApi(
                RetrofitClient().updateProfile(
                    SharedPref.getUSERTOKEN().toString(),
                    sfname.getReqBody(),
                    semail.getReqBody(),
                    scity.getReqBody(),
                    sflate.getReqBody(),
                    saddress.getReqBody(),
                    spinCode.getReqBody(),
                    profileUri.getReqBody("profile_pic")
                )
            ) { responseReg(it) }
        }

    }

    private fun responseReg(it: Any) {
        when (it) {
            is LoginSignupResponse -> {
                toast(it.msg)
                if (!it.error!!) {
                    it.data?.run {
                        SharedPref.run {
                            setUSERTOKEN(token.toString())
                            setSKIP(false)
                            setUSER_NAME(name.toString())
                            setUSER_NAME(name.toString())
                            setUSER_ID(id.toString())
                            setUSER_EMAIL(email.toString())
                            setUSER_MOB(mobile.toString())
                            setUSER_PIC(profilePic.toString())
                            setADDRESS_CITY(city.toString())
                            setADDRESS_ZIPCODE(postcode.toString())
                            setADDRESS_HOUSE(address.toString())
                            setADDRESS_ADDRESS(address.toString())
                        }
                        finish()
                    }
                }

            }
            is CommonResponse -> {
                if (!it.error!!) {
                    finish()
                }
            }

        }
    }

    private fun takePicture() {
        next.hideSoftKeyboard()
        ImagePicker.with(this)
            //.cropSquare()
            .compress(512)         //Final image size will be less than 1 MB(Optional)
            .galleryMimeTypes(arrayOf("image/png", "image/jpeg", "image/jpg"))
            .maxResultSize(
                540,
                1080
            )  //Final image resolution will be less than 1080 x 1080(Optional)
            .createIntent { intent ->
                showDialog()
                startForPicResult.launch(intent)
            }
    }

    private val startForPicResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
            val resultCode = result.resultCode
            val data = result.data
            log(msg = "Result--->")
            when (resultCode) {
                Activity.RESULT_OK -> {
                    profileUri = data?.data!!

                    data?.data?.let { profile_pic.setImg(it, 100) }
                    log(msg = "Result--- after set img>")
                }
            }
        }

}