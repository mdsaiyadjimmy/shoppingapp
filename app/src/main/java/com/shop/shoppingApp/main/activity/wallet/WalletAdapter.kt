package com.shop.shoppingApp.main.activity.wallet


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.shop.shoppingApp.R
import com.shop.shoppingApp.model.wallet.WalletItem
import com.shop.shoppingApp.util.capsFirstWord
import com.shop.shoppingApp.util.hide
import com.shop.shoppingApp.util.setAmount
import com.shop.shoppingApp.util.show
import kotlinx.android.synthetic.main.item_wallet.view.*

class WalletAdapter(val context: Context, var mainlist: MutableList<WalletItem>) :
    RecyclerView.Adapter<WalletAdapter.MyViewHolder>() {


    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): MyViewHolder {
        return MyViewHolder(
            LayoutInflater.from(p0.context).inflate(
                R.layout.item_wallet,
                p0,
                false
            )
        )
    }

    override fun getItemCount() = mainlist.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.itemView.apply {
            mainlist[position].run {
                item_wallet_comment.text = description.toString().capsFirstWord()
                item_wallet_id.text = id?.toString()
                item_wallet_amnt.text = pointvalue.setAmount()
                item_wallet_date.text = refrenceId.toString()
                setOnClickListener {
                    if (ll_item_wallet_comment.isVisible) ll_item_wallet_comment.hide()
                    else ll_item_wallet_comment.show()
                }
//        item_wallet_view.setOnClickListener {
//            viewImagesAPi(
//                RetrofitClient.getClient().getExpenseImgs(
//                    SharedPref(context).getACCESSTOKEN().toString(),
//                    id.toString()
//                )
//            )
//        }

            }
        }

    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)


}