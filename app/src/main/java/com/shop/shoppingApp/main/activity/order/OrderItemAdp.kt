package com.shop.shoppingApp.main.activity.order

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.shop.shoppingApp.R
import com.shop.shoppingApp.model.orders.OrderItem
import com.shop.shoppingApp.util.capsWord
import com.shop.shoppingApp.util.getAmount
import com.shop.shoppingApp.util.setAmount
import com.shop.shoppingApp.util.setImg
import kotlinx.android.synthetic.main.item_order_det.view.*

class OrderItemAdp( var mainlist: MutableList<OrderItem>) :
    RecyclerView.Adapter<OrderItemAdp.MyViewHolder>() {


    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): MyViewHolder {
        return MyViewHolder(
            LayoutInflater.from(p0.context).inflate(
                R.layout.item_order_det,
                p0,
                false
            )
        )
    }

    override fun getItemCount() = mainlist.size


    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.itemView.apply {
            mainlist[position].run {
                item_order_detail_img.setImg(img)
                item_order_detail_title.text = productName?.capsWord()
                item_order_detail_qty.text = "x $qty"
                item_order_detail_price.text = (price.getAmount()*qty.toDouble()).toString().setAmount()
                item_order_detail_price_per_item.text = price.setAmount()
            }
        }


    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)


    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

}