package com.shop.shoppingApp.main.activity

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.app.ActivityOptionsCompat
import com.shop.shoppingApp.R
import com.shop.shoppingApp.api.appLink
import com.shop.shoppingApp.main.BaseActivtiy
import com.shop.shoppingApp.util.SharedPref
import com.shop.shoppingApp.util.Util
import com.shop.shoppingApp.util.noStatusNoNav
import kotlinx.android.synthetic.main.actionbar_primary.*
import kotlinx.android.synthetic.main.activity_splash.*
import kotlinx.android.synthetic.main.refer_n_earn.*

class ReferNEarn : BaseActivtiy() {
    var code = "ZAYA6060"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.refer_n_earn)
        refer_n_earn_share.setOnClickListener {
            shareLink()
        }
        action_bar_title.text = "Refer and Earn"
        back.setOnClickListener { finish() }

    }

    private fun shareLink() {

        val shareIntent = Intent()
        shareIntent.action = Intent.ACTION_SEND

        shareIntent.putExtra(
            Intent.EXTRA_TEXT,
            "\n Download the App now->\n $appLink\n\n Use this code on your first order $code"
        )
        shareIntent.type = "text/plain"
        startActivity(Intent.createChooser(shareIntent, "Share with"))
    }


}
