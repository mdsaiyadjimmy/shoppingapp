package com.shop.shoppingApp.main.fragment.cart


import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.shop.shoppingApp.R
import com.shop.shoppingApp.api.RetrofitClient
import com.shop.shoppingApp.main.BaseFragment
import com.shop.shoppingApp.model.cart.CartItem
import com.shop.shoppingApp.model.commonModel.CommonResponse
import com.shop.shoppingApp.util.*
import kotlinx.android.synthetic.main.item_cart.view.*

class CartAdapter(
    var mainlist: MutableList<CartItem>,
    var cart: (qty: Int,pos:Int) -> Unit
) : RecyclerView.Adapter<CartAdapter.MyViewHolder>() {
    lateinit var ctx: Context

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): MyViewHolder {
        ctx = p0.context
        return MyViewHolder(LayoutInflater.from(p0.context).inflate(R.layout.item_cart, p0, false))
    }

    override fun getItemCount(): Int = mainlist.size


    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.itemView.apply {
            mainlist[position].run {
                item_cart_img.setImg(img)
                item_cart_title.text = productName?.capsWord()
                item_cart_price_per_item.text = price?.setAmount()
                item_cart_price.text = (price!!.getAmount() * qty.toDouble()).toString().setAmount()
                item_cart_qty.text = qty.toString()
                item_cart_plus.setOnClickListener {
                    cart(qty + 1,position)
                }
                item_cart_minus.setOnClickListener {
                    if (qty > 1) cart(qty - 1,position)
                    else ctx.optionDialog("Remove item") {
                        if (it) cart(0,position)
                    }
                }
                item_delete.setOnClickListener {
                    ctx.optionDialog("Remove item") {
                        if (it)  cart(0,position)
                    }
                }
                setOnClickListener {
//                (context as BaseFragment).startActivity(AddAddress())
                }
            }
        }
    }



    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

}