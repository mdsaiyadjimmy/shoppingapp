package com.shop.shoppingApp.main.activity

import android.content.Intent
import android.os.Bundle
import androidx.core.app.ActivityOptionsCompat
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.shop.shoppingApp.R
import com.shop.shoppingApp.ShoppingApp.Companion.cartCount
import com.shop.shoppingApp.ShoppingApp.Companion.isFromOrder
import com.shop.shoppingApp.main.BaseActivtiy
import com.shop.shoppingApp.main.activity.product.Product
import com.shop.shoppingApp.util.CartCount
import com.shop.shoppingApp.util.SharedPref
import com.shop.shoppingApp.util.hide
import com.shop.shoppingApp.util.pushDownANim.PushDownAnim
import com.shop.shoppingApp.util.whiteStatusBar
import kotlinx.android.synthetic.main.actionbar_home.*
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivtiy(), CartCount {
    lateinit var navController: NavController
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        whiteStatusBar()
        setContentView(R.layout.activity_main)
        cartCount = this
        navController = findNavController(R.id.nav_host_fragment_activity_main)
//        // Passing each menu ID as a set of Ids because each
//        // menu should be considered as top level destinations.
//        val appBarConfiguration = AppBarConfiguration(
//            setOf(
//                R.id.navigation_home,
//                R.id.navigation_dashboard,
//                R.id.navigation_notifications
//            )
//        )

        search.setOnClickListener {
            val opt = ActivityOptionsCompat.makeSceneTransitionAnimation(
                c,
                search, "search"
            )
            startActivity(
                Intent(c, Product::class.java).putExtra("isSearch", "yes"),
                opt.toBundle()
            )
        }
        back.hide()
        back.setOnClickListener {
            finish()
        }

        action_bar_title.setOnClickListener {
            finishAffinity()
            SharedPref.clearSession()
            startActivity(Splash())
        }
        nav_view.setupWithNavController(navController)
        if (isFromOrder) navController.navigate(R.id.navigation_order)

//        callApi(RetrofitClient().getProductHomepage()) {
//            when (it) {
//                is ProductHomepageResponse -> {
//                    toast("${it.message}")
//                    log(it.toString())
//                }
//            }
//        }
    }

    override fun getCount(count: Int) {
        nav_view.getOrCreateBadge(R.id.navigation_cart)?.number = count
    }

    override fun goToHome() {
        navController.navigate(R.id.navigation_home)
    }




}