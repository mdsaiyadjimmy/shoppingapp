package com.shop.shoppingApp.main

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.shop.shoppingApp.util.MyDialogBox
import com.shop.shoppingApp.util.NetworkUtil
import com.shop.shoppingApp.api.callApiG
import com.shop.shoppingApp.util.log
import retrofit2.Call

open class BaseFragment : Fragment() {
    var dialogBox: MyDialogBox? = null

    lateinit var c: Context
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        c = requireContext()
    }

    fun <T : Any> callApi(call: Call<T>, showDialog: Boolean = true, fxn: (res: Any) -> Unit) {
        if (NetworkUtil.isConnected()) {
            if (showDialog) showDialog()
            callApiG(call) { res, isSuccess ->
                if (isSuccess) fxn(res)
                log(msg = "isSuccess $isSuccess")
                dismissDialog()
            }
        }
    }

    fun showDialog() {
        dialogBox?.run {
            if (!this.isShowing) show()
        } ?: run {
            dialogBox = MyDialogBox(c)
            dialogBox?.show()
        }
    }

    fun dismissDialog() {

        dialogBox?.run {
            if (this.isShowing) dismiss()
        } ?: run {
            dialogBox = MyDialogBox(c)
        }
    }

    fun startActivity(activity: Activity) {
        startActivity(Intent(c, activity::class.java))
    }
}