package com.shop.shoppingApp.main.fragment.home.adp


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.shop.shoppingApp.R
import com.shop.shoppingApp.util.capsWord
import com.shop.shoppingApp.util.pushDownANim.PushDownAnim
import kotlinx.android.synthetic.main.item_offer.view.*


class OfferAdp(val context: Context, var mainlist: MutableList<String>) :
    RecyclerView.Adapter<OfferAdp.MyViewHolder>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): MyViewHolder {
        return MyViewHolder(
            LayoutInflater.from(p0.context).inflate(
                R.layout.item_offer,
                p0,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return mainlist.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        PushDownAnim.setPushDownAnimTo(holder.itemView).setOnClickListener {
//
//           if (mainlist[position].productOffer){
//               context.startActivity(
//                   Intent(context, ProductDetail::class.java)
//                       .putExtra("slug", mainlist[position].productId?.slug.toString())
//                       .putExtra("name", mainlist[position].title.toString())
//               )
//           }
//            else  if (mainlist[position].categoryOffer){
//               context.startActivity(
//                   Intent(context, SecSubCatList::class.java)
//                       .putExtra("data", Gson().toJson(mainlist[position].productSubCategory)))
//
//           }
        }
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }
}