package com.shop.shoppingApp.main.fragment.home.adp


import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.IntentFilter.create
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.core.app.ActivityOptionsCompat
import androidx.customview.widget.ViewDragHelper.create
import androidx.recyclerview.widget.RecyclerView
import androidx.vectordrawable.graphics.drawable.VectorDrawableCompat.create
import com.google.gson.Gson
import com.shop.shoppingApp.R
import com.shop.shoppingApp.ShoppingApp.Companion.cartCount
import com.shop.shoppingApp.api.RetrofitClient
import com.shop.shoppingApp.main.BaseActivtiy
import com.shop.shoppingApp.main.activity.product.ProductDetail
import com.shop.shoppingApp.model.cart.AddToCartResponse
import com.shop.shoppingApp.model.commonModel.CommonResponse
import com.shop.shoppingApp.model.productHomepage.TopProductsItem
import com.shop.shoppingApp.util.*
import com.shop.shoppingApp.util.pushDownANim.PushDownAnim
import kotlinx.android.synthetic.main.activity_splash.*
import kotlinx.android.synthetic.main.item_category.view.*
import kotlinx.android.synthetic.main.item_product.view.*
import java.net.URI.create

class ProductAdapter(var mainlist: MutableList<TopProductsItem>) :
    RecyclerView.Adapter<ProductAdapter.MyViewHolder>() {
    lateinit var ctx: Context


    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): MyViewHolder {
        ctx = p0.context
        return MyViewHolder(
            LayoutInflater.from(p0.context).inflate(R.layout.item_product, p0, false)
        )
    }

    override fun getItemCount(): Int {
        return mainlist.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.itemView.apply {
            mainlist[position].let {
                item_product_img.setImg(it.img)
                ratingbar.rating = 5.0f
                item_product_title.text = it.name?.capsWord()
                item_product_desc.text = "By " + it.brand?.capsWord()


                this.setPrice(it, 0)
                sp_unit.adapter = getadp(it.sizes.getList())
                sp_unit.setSelection(0, false)
                sp_unit.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                    override fun onItemSelected(
                        p0: AdapterView<*>?,
                        p1: View?,
                        pos: Int,
                        p3: Long
                    ) {
                        log(msg = "Selected pos $pos")
                        holder.itemView.setPrice(it, pos)
                    }

                    override fun onNothingSelected(p0: AdapterView<*>?) {
                    }

                }

                PushDownAnim.setPushDownAnimTo(this).setOnClickListener { _ ->
//                    val v1 = Pair.create(item_product_title as View, "title")
//                    val v2 = Pair.create(item_product_img as View, "img")
                    val opt = ActivityOptionsCompat.makeSceneTransitionAnimation(
                        (ctx as Activity),
                        item_product_img, "img"
                    )
                    ctx.startActivity(
                        Intent(ctx, ProductDetail::class.java)
                            .putExtra("data", Gson().toJson(it)), opt.toBundle()
                    )
                }

                addToCart.setOnClickListener { _ ->
                    (ctx as BaseActivtiy).callApi(
                        RetrofitClient().addToCart(
                            SharedPref.getUSERTOKEN(),
                            it.id.toString(),
                            it.variantId.getList()[it.pos], "1"
                        )
                    ) { res ->
                        if (res is AddToCartResponse) {
                            if (!res.error) {
                                toast("Item Added to Cart")
                                cartCount.getCount(res.data.size)
                            }
                        }
                    }

                }
            }
        }

    }

    private fun View.setPrice(item: TopProductsItem, pos: Int) {
        item.pos = pos
        var mrp = item.mrps.getList()[pos]
        var dPrice = item.sps.getList()[pos]
        item_product_price.text = dPrice.setAmount()
        item_product_disc_price.apply {
            text = mrp.setAmount()
            paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
            if (mrp == dPrice) hide()
            else show()
        }
        var off = getpercent(mrp.getAmount(), dPrice.getAmount())
        if (off > 0) {
            item_product_offer.text = "$off%\nOff"
            item_product_offer.show()
        } else item_product_offer.hide()

    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

}