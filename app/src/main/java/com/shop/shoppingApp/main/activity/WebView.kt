package com.shop.shoppingApp.main.activity

import android.graphics.Bitmap
import android.os.Bundle
import android.util.Log
import android.webkit.WebResourceError
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import com.shop.shoppingApp.R
import com.shop.shoppingApp.main.BaseActivtiy
import com.shop.shoppingApp.util.NetworkUtil
import com.shop.shoppingApp.util.toast
import kotlinx.android.synthetic.main.actionbar_primary.*
import kotlinx.android.synthetic.main.activity_webview.*

class WebView : BaseActivtiy() {
    var isLoaded = false
    var webURL = "https://zayadryfruits.com"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_webview)
        action_bar_title.text = intent.getStringExtra("from")
        webURL = intent.getStringExtra("to").toString()
        back.setOnClickListener {
            onBackPressed()
        }
    }

    override fun onResume() {
        if (NetworkUtil.isConnected() && !isLoaded) loadWebView()
        super.onResume()
    }

    private fun loadWebView() {
        webView.loadUrl(webURL)
        webView.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(
                view: WebView?,
                request: WebResourceRequest?
            ): Boolean {
                Log.d("TAG", "shouldOverrideUrlLoading:")

                val url = request?.url.toString()
                view?.loadUrl(url)
                return super.shouldOverrideUrlLoading(view, request)
            }

            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                Log.d("TAG", "onPageStarted:")

                super.onPageStarted(view, url, favicon)
                showDialog()

            }

            override fun onPageFinished(view: WebView?, url: String?) {

                super.onPageFinished(view, url)
                dismissDialog()

                isLoaded = true
            }

            override fun onReceivedError(
                view: WebView,
                request: WebResourceRequest,
                error: WebResourceError
            ) {
                isLoaded = false
                val errorMessage = "Got Error! $error"
                toast(errorMessage)
                dismissDialog()
                super.onReceivedError(view, request, error)
            }
        }
    }


    override fun onBackPressed() {
        if (webView.canGoBack()) {
            webView.goBack()
        } else {
            super.onBackPressed()
        }
    }

}