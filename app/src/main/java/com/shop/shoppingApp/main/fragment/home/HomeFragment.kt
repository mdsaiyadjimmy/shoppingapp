package com.shop.shoppingApp.main.fragment.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import androidx.recyclerview.widget.RecyclerView
import com.shop.shoppingApp.R
import com.shop.shoppingApp.ShoppingApp
import com.shop.shoppingApp.api.RetrofitClient
import com.shop.shoppingApp.main.BaseFragment
import com.shop.shoppingApp.main.activity.MainActivity
import com.shop.shoppingApp.main.activity.Splash
import com.shop.shoppingApp.main.activity.category.Category
import com.shop.shoppingApp.main.fragment.home.adp.*
import com.shop.shoppingApp.model.productHomepage.*
import com.shop.shoppingApp.util.*
import com.shop.shoppingApp.util.pushDownANim.PushDownAnim
import kotlinx.android.synthetic.main.fragment_home.*

class HomeFragment : BaseFragment() {


    var catlist = mutableListOf<CategoriesItem>()
    var mainlist = mutableListOf<TopProductsItem>()
    var bannerlist = mutableListOf<BannersItem>()

    //    var offerlist = mutableListOf<OfferListItem>()
//    var subcatlist = mutableListOf<ProductSubCategoryListItem>()
    var loading = true
    var text = ""
    var visibleItemCount = 0
    var totalItemCount = 0
    var pastVisiblesItems = 0
    var pagination = 0
    lateinit var layoutManager: GridLayoutManager
    lateinit var cadpter: CatAdp
    lateinit var adpter: ProductAdapter
    lateinit var oadpter: OfferAdp

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    private fun init() {

//        }
        product_home_cat_more.setOnClickListener {
            startActivity(Category())
        }
        product_home_sub_cat_more.setOnClickListener {
            // startActivity(Category())
        }
        product_home_offer_more.setOnClickListener {
            //startActivity(Category())
        }


        cadpter = CatAdp(catlist)
        adpter = ProductAdapter(mainlist)
//        oadpter = OfferAdp(c, offerlist)
//        sadpter = HomeSubCatAdp(c, subcatlist)
        layoutManager = GridLayoutManager(c, 2)
        rv_product.addItemDecoration(GridSpacingItemDecoration(2, 5, false))
        rv_product.layoutManager = layoutManager
        rv_product.adapter = adpter
        rv_categories.layoutManager = GridLayoutManager(c, 2, GridLayoutManager.HORIZONTAL, false)
        rv_categories.adapter = cadpter
        rv_offers.layoutManager = LinearLayoutManager(c, LinearLayoutManager.HORIZONTAL, false)
//        rv_offers.adapter = oadpter
        rv_top_cat.layoutManager = GridLayoutManager(c, 3)

        //val snapHelper: SnapHelper = PagerSnapHelper()
        PagerSnapHelper().attachToRecyclerView(rv_offers)



        ll_swipe.setOnRefreshListener {
            getproject()
        }
        getproject()

        rv_product.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {


                if (dy > 0 && catlist.size > 20) {
                    visibleItemCount = layoutManager.childCount
                    totalItemCount = layoutManager.itemCount
                    pastVisiblesItems =
                        (rv_product.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()
                    log("TAG", "totalItemCount: $totalItemCount")
                    if (loading) {
                        if (visibleItemCount + pastVisiblesItems >= totalItemCount) {
                            loading = false
                            pagination++
                            // getproject(pagination)
                        }
                    }
                }
            }
        })

    }

    fun getproject() {
        ll_swipe.isRefreshing = false
        callApi(RetrofitClient().getProductHomepage(SharedPref.getUSERTOKEN())) {
            if (it is HomepageResponse) {
                log(msg = "isSuccess2 ")
                try {
                    if (!it.error!!) {

                        main?.show()

                        it.data?.run {
                            ShoppingApp.cartCount.getCount(cartCount)
                            mainlist.clear()
                            catlist.clear()
//                        subcatlist.clear()
//                        offerlist.clear()
                            bannerlist.clear()
                            catlist.addAll(categories)
                            mainlist.addAll(topProducts)
//                        subcatlist.addAll(it.productSubCategoryList)
//                        offerlist.addAll(it.offerList)
                            bannerlist.addAll(banners)
                            if (bannerlist.isNotEmpty()) {
                                indicator?.detachFromPager()
                                product_detail_auto_scroll.adapter =
                                    ImageAutoScrollPagerHomeAdp(bannerlist)
                                product_detail_auto_scroll.startAutoScroll()
                                indicator.attachToPager(product_detail_auto_scroll)
                                ll_banner.show()
                            } else ll_banner.hide()

//                        if (offerlist.isNotEmpty()) offer_indicator.attachToRecyclerView(
//                            rv_offers
//                        )
//                        else offer_ll.hide()
//                        if (subcatlist.isEmpty()) home_top_service_card.hide()
//                        else home_top_service_card.show()
                            log(msg = "isSuccess3 ")
                            adpter.notifyDataSetChanged()
                            cadpter.notifyDataSetChanged()
//                        oadpter.notifyDataSetChanged()
//                        sadpter.notifyDataSetChanged()
                        }
                    } else {
                        if (it.msg!!.contains("Unauthorized request")) {
                            SharedPref.clearSession()
                            (c as MainActivity).finishAffinity()
                            startActivity(Splash())
                            toast("Session Expired! Please Login again.")
                        }
                    }
                } catch (e: Exception) {
                }
            }
        }


    }

}