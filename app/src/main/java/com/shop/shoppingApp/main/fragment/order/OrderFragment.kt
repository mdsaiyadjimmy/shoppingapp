package com.shop.shoppingApp.main.fragment.order

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.shop.shoppingApp.R
import com.shop.shoppingApp.api.RetrofitClient
import com.shop.shoppingApp.main.BaseFragment
import com.shop.shoppingApp.model.commonModel.CommonResponse
import com.shop.shoppingApp.model.orders.OrderItems
import com.shop.shoppingApp.model.orders.OrdersResponse
import com.shop.shoppingApp.util.*
import kotlinx.android.synthetic.main.fragment_order.*
import kotlinx.android.synthetic.main.no_product_found.*

class OrderFragment : BaseFragment() {
    lateinit var layoutManager: LinearLayoutManager
    val orderList = mutableListOf<OrderItems>()
    lateinit var adpter: OrderAdp
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_order, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        layoutManager = LinearLayoutManager(c)
        adpter = OrderAdp(orderList) { pos ->
            c.optionDialog("Cancel this Order") {
                if (it) {
                    callApi(RetrofitClient().cancelOrder(orderList[pos].id.toString())) {
                        if (it is CommonResponse) {
                            toast(it.message)
                            orderList[pos].status = "cancelled"
                            adpter.notifyDataSetChanged()
                        }
                    }
                }
            }
        }
        rv.layoutManager = layoutManager
        rv.adapter = adpter
        swipe.setOnRefreshListener {
            getList()
        }
    }

    override fun onResume() {
        super.onResume()
        getList()
    }

    private fun getList() {
        swipe.isRefreshing = false
        callApi(
            RetrofitClient().getOrders(SharedPref.getUSERTOKEN())
        ) {
            if (it is OrdersResponse) {
                if (!it.error) {
                    orderList.clear()
                    orderList.addAll(it.data)
                    adpter.notifyDataSetChanged()
                    if (orderList.size > 0) ll_no_record.hide()
                    else ll_no_record.show()
                    error_img.setImg(R.drawable.shopping_bag)
                    no_product_title.text = "No Order found!"
                    start_shopping.hide()
                }
            }
        }
    }
}