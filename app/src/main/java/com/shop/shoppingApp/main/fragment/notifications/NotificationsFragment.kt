package com.shop.shoppingApp.main.fragment.notifications

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.shop.shoppingApp.R
import com.shop.shoppingApp.api.RetrofitClient
import com.shop.shoppingApp.main.BaseFragment
import com.shop.shoppingApp.main.fragment.order.OrderAdp
import com.shop.shoppingApp.model.commonModel.CommonResponse
import com.shop.shoppingApp.model.notification.NotifItem
import com.shop.shoppingApp.model.notification.NotificationResponse
import com.shop.shoppingApp.model.orders.OrderItems
import com.shop.shoppingApp.model.orders.OrdersResponse
import com.shop.shoppingApp.util.*
import kotlinx.android.synthetic.main.frag_cart.*
import kotlinx.android.synthetic.main.fragment_order.*
import kotlinx.android.synthetic.main.fragment_order.ll_no_record
import kotlinx.android.synthetic.main.no_product_found.*

class NotificationsFragment : BaseFragment() {

    lateinit var layoutManager: LinearLayoutManager
    val mainlist = mutableListOf<NotifItem>()
    lateinit var adpter: NotifAdapter
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_order, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        layoutManager = LinearLayoutManager(c)

        adpter = NotifAdapter(c, mainlist)
        //{ pos ->
//            c.optionDialog("Cancel this Order") {
//                if (it) {
//                    callApi(RetrofitClient().cancelOrder(mainlist[pos].id.toString())) {
//                        if (it is CommonResponse) {
//                            toast(it.message)
//                            mainlist[pos].status = "cancelled"
//                            adpter.notifyDataSetChanged()
//                        }
//                    }
//                }
//            }
//        }
        rv.layoutManager = layoutManager
        rv.adapter = adpter
        swipe.setOnRefreshListener {
            getList()
        }
    }

    override fun onResume() {
        super.onResume()
        getList()
    }

    private fun getList() {
        swipe.isRefreshing = false
        callApi(
            RetrofitClient().getNotif(SharedPref.getUSERTOKEN())
        ) {
            if (it is NotificationResponse) {
                if (!it.error) {
                    mainlist.clear()
                    mainlist.addAll(it.data)
                    adpter.notifyDataSetChanged()
                    if (mainlist.size > 0) ll_no_record.hide()
                    else ll_no_record.show()
                    error_img.setImg(R.drawable.ic_notifications_black_24dp)
                    no_product_title.text = "Notification not found"
                    start_shopping.hide()
                }
            }
        }
    }
}