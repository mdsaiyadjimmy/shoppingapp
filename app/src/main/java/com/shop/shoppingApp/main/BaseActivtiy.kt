package com.shop.shoppingApp.main

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import androidx.appcompat.app.AppCompatActivity
import com.shop.shoppingApp.util.MyDialogBox
import com.shop.shoppingApp.util.NetworkUtil
import com.shop.shoppingApp.api.callApiG
import com.shop.shoppingApp.util.whiteStatusBar
import retrofit2.Call

open class BaseActivtiy : AppCompatActivity() {
    var dialogBox: MyDialogBox? = null

     var c = this@BaseActivtiy

    fun <T : Any> callApi(call: Call<T>, showDialog: Boolean = true, fxn: (res: Any) -> Unit) {
        if (NetworkUtil.isConnected()) {
            if (showDialog) showDialog()
            callApiG(call) { res, isSucces ->
                if (isSucces) fxn(res)
                dismissDialog()
            }
        }
    }

    fun showDialog() {
        dialogBox?.run {
            if (!this.isShowing) show()
        } ?: run {
            dialogBox = MyDialogBox(c)
            dialogBox?.show()
        }
    }

    fun dismissDialog() {
        dialogBox?.run {
            if (this.isShowing) dismiss()
        } ?: run {
            dialogBox = MyDialogBox(c)
        }
    }
    fun startActivity(activity: Activity) { startActivity(Intent(c,activity::class.java))
    }
}