package com.shop.shoppingApp.main.activity.adress

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.shop.shoppingApp.R
import com.shop.shoppingApp.model.addressModel.DataItem
import com.shop.shoppingApp.util.*
import kotlinx.android.synthetic.main.item_address.view.*

class AddressAdapter(var mainlist: MutableList<DataItem>, val fxn: (pos: Int) -> Unit) :
    RecyclerView.Adapter<AddressAdapter.MyViewHolder>() {
    var lastpos = -1
    lateinit var selectedAdd: LinearLayout

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): MyViewHolder {
        return MyViewHolder(
            LayoutInflater.from(p0.context).inflate(R.layout.item_address, p0, false)
        )
    }

    override fun getItemCount(): Int=5// mainlist.size


    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int = position

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        //  holder.itemView.item_cart_sp.adapter=Util.spUnit(context)
        /*holder.itemView.apply {


            item_address_type.text = mainlist[position].addressType
            item_address_name.text = mainlist[position].firstName
            item_address__house_no.text = mainlist[position].houseNo
            item_address__street.text = mainlist[position].street
            item_address_pin.text = mainlist[position].pincode
            item_address__contact.text = mainlist[position].contactNo

            row.setOnClickListener {
                if (lastpos < 0) {
                    lastpos = position
                    selectedAdd = holder.itemView.item_address_rb
                    item_address_rb.show()
                    fxn(position)

                } else {

                    if (item_address_rb.isVisible) {
                        lastpos = -1
                        item_address_rb.invisible()
                        fxn(-1)
                    } else {
                        selectedAdd.invisible()
                        item_address_rb.show()
                        fxn(position)
                        lastpos = position
                        selectedAdd = holder.itemView.item_address_rb
                    }

                }
            }

            item_address_delete.setOnClickListener {
                optionDialog("Delete?") {
                    toast("here$it")
                }
            }

            item_address_edit.setOnClickListener {

                context.startActivity(
                    Intent(context, AddAddress::class.java)
                        .putExtra("edit", true)
                        .putExtra("address_data", mainlist[position])

                )
            }
        }*/
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)


}