package com.shop.shoppingApp.main.activity.category

import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.shop.shoppingApp.R
import com.shop.shoppingApp.api.RetrofitClient
import com.shop.shoppingApp.main.BaseActivtiy
import com.shop.shoppingApp.main.fragment.home.adp.CatAdp
import com.shop.shoppingApp.model.category.CategoryResponse
import com.shop.shoppingApp.model.productHomepage.CategoriesItem
import com.shop.shoppingApp.model.products.ProductResponse
import com.shop.shoppingApp.util.SharedPref
import com.shop.shoppingApp.util.whiteStatusBar
import kotlinx.android.synthetic.main.actionbar_primary.*
import kotlinx.android.synthetic.main.activity_product.*

class Category : BaseActivtiy() {
    lateinit var glm: GridLayoutManager
    var mainlist = mutableListOf<CategoriesItem>()
    lateinit var adpter: CatAdp
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        whiteStatusBar()
        setContentView(R.layout.activity_product)
        adpter = CatAdp(mainlist)
        glm = GridLayoutManager(c, 3, GridLayoutManager.VERTICAL, false)
        rv.apply {
            layoutManager = glm
            adapter = adpter
        }
        back.setOnClickListener { finish() }
        action_bar_title.text="Top Categories"
        getCat()
    }

    private fun getCat() {
        callApi(
            RetrofitClient().getcat(
                SharedPref.getUSERTOKEN()
            )
        ) {
            if (it is CategoryResponse) {
                if (!it.error) {
                    mainlist.clear()
                    mainlist.addAll(it.data)
                    adpter.notifyDataSetChanged()
                }
            }
        }
    }


}