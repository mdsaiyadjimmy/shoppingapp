package com.shop.shoppingApp.main.fragment.cart

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.shop.shoppingApp.R
import com.shop.shoppingApp.ShoppingApp.Companion.cartCount
import com.shop.shoppingApp.api.RetrofitClient
import com.shop.shoppingApp.main.BaseFragment
import com.shop.shoppingApp.main.activity.PaymentActivity
import com.shop.shoppingApp.main.activity.adress.AddressList
import com.shop.shoppingApp.model.cart.AddToCartResponse
import com.shop.shoppingApp.model.cart.CartItem
import com.shop.shoppingApp.model.cart.CartResponse
import com.shop.shoppingApp.model.commonModel.CommonResponse
import com.shop.shoppingApp.util.*
import kotlinx.android.synthetic.main.frag_cart.*
import kotlinx.android.synthetic.main.no_product_found.*

class CartFrag : BaseFragment() {
    var mainlist = mutableListOf<CartItem>()
    var sum = 0.0
    lateinit var layoutManager: LinearLayoutManager
    lateinit var adpter: CartAdapter
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.frag_cart, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        c = activity as Context
//        MainActivity.txt_home_title.text = "Cart"


        adpter = CartAdapter(mainlist) { qty, pos ->

            addTocart(mainlist[pos], qty)

        }
        layoutManager = LinearLayoutManager(c)
        rv_cart.layoutManager = layoutManager
        rv_cart.adapter = adpter



        start_shopping.setOnClickListener {

        }

        checkout.setOnClickListener {
            startActivity(PaymentActivity())
        }

    }

    private fun addTocart(cartItem: CartItem, qty: Int) {

        callApi(
            RetrofitClient().addToCart(
                SharedPref.getUSERTOKEN(),
                cartItem.id.toString(),
                cartItem.variantId.toString(),
                qty.toString()
            )
        ) { res ->
            if (res is AddToCartResponse) {
                if (!res.error) {
                    toast("Item Added to Cart")
                    if (qty == 0) mainlist.remove(cartItem)
                    else cartItem.qty = qty
                    adpter.notifyDataSetChanged()
                    setTotal()

                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        getList()
    }

    private fun getList() {
        callApi(
            RetrofitClient().getCart(SharedPref.getUSERTOKEN())
        ) {
            if (it is CartResponse) {
                if (!it.error) {
                    mainlist.clear()
                    mainlist.addAll(it.data)
                    adpter.notifyDataSetChanged()
                    setTotal()
                }
            }
        }
    }

    fun setTotal() {
        sum = 0.0
        cartCount.getCount(mainlist.size)
        mainlist.forEach {
            sum += it.qty.toDouble() * it.price.getAmount()
        }
        if (mainlist.size > 0) {
            cart_total.text = sum.toString().setAmount()
            ll_checkout.show()
            cart_ll_all.show()

        } else {
            ll_no_record.show()
            ll_checkout.hide()
        }

    }

}
