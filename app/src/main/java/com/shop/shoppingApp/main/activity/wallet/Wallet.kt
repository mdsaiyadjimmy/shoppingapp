package com.shop.shoppingApp.main.activity.wallet

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.piashsarker.www.easy_utils_lib.Utils
import com.shop.shoppingApp.R
import com.shop.shoppingApp.api.RetrofitClient
import com.shop.shoppingApp.main.BaseActivtiy
import com.shop.shoppingApp.model.loginModel.LoginSignupResponse
import com.shop.shoppingApp.model.wallet.WalletItem
import com.shop.shoppingApp.model.wallet.WalletResponse
import com.shop.shoppingApp.util.*
import kotlinx.android.synthetic.main.actionbar_primary.*
import kotlinx.android.synthetic.main.filter_dialog.*
import kotlinx.android.synthetic.main.wallet.*
import kotlinx.android.synthetic.main.wallet.rv

class Wallet : BaseActivtiy() {
    var loading = false
    var key = ""
    var toDate = ""
    var fromDate = ""
    var canSearch = true
    var mainlist = mutableListOf<WalletItem>()
    lateinit var layoutManager: LinearLayoutManager
    lateinit var adpter: WalletAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        whiteStatusBar()
        setContentView(R.layout.wallet)
        adpter = WalletAdapter(c, mainlist)
        action_bar_title.text = "Wallet"
        layoutManager = LinearLayoutManager(c)
        rv.layoutManager = layoutManager
        rv.adapter = adpter

        back.setOnClickListener {
            onBackPressed()
        }
        filter.setOnClickListener {
            filterDialog()
        }

        toDate = Utils.getCurrentDateTime("yyyy-MM-dd")
        fromDate = Utils.getCurrentDateTime("yyyy-MM") + "-01"


    }

    override fun onResume() {
        super.onResume()
        getList()
        getProfile()
    }

    private fun getList() {
        callApi(
            RetrofitClient().getWallet(SharedPref.getUSERTOKEN())
        ) {
            if (it is WalletResponse) {
                if (!it.error) {
                    mainlist.clear()
                    mainlist.addAll(it.data)
                    adpter.notifyDataSetChanged()
                }
            }
        }
    }
    fun getProfile() {
        callApi(RetrofitClient().getProfile(SharedPref.getUSERTOKEN())) {
            if (it is LoginSignupResponse) {
                if (!it.error!!) {
                   // ll_note.show()
                    wallet_total.text = it.data?.wallet.setAmount()
                   // if (it.data?.wallet.getAmount() > 0) ll_cb_wallet.show()
                   // else ll_cb_wallet.hide()
                }
            }
        }
    }

    fun filterDialog() {
        var d = BottomSheetDialog(c).apply {
            setContentView(R.layout.filter_dialog)
            from_date.setOnClickListener {
                from_date.getdate()
            }
            to_date.setOnClickListener {
                to_date.getdate()
            }
            from_date.text = fromDate
            to_date.text = toDate
            dialog_yes.setOnClickListener {
                fromDate = from_date.text.toString().trim()
                toDate = to_date.text.toString().trim()
                if (fromDate.isEmpty() || toDate.isEmpty()) toast(
                    "Need to Select from and to date both!"
                )
                else {
                    dismiss()

                }
            }

            dialog_no.setOnClickListener {
                dismiss()
            }
        }
        d.show()
    }
}