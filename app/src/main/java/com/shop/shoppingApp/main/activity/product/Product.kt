package com.shop.shoppingApp.main.activity.product

import android.os.Bundle
import android.util.Log
import android.view.inputmethod.EditorInfo
import androidx.core.widget.doAfterTextChanged
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.shop.shoppingApp.R
import com.shop.shoppingApp.api.RetrofitClient
import com.shop.shoppingApp.main.BaseActivtiy
import com.shop.shoppingApp.main.fragment.home.adp.CatAdp
import com.shop.shoppingApp.main.fragment.home.adp.ProductAdapter
import com.shop.shoppingApp.model.productHomepage.CategoriesItem
import com.shop.shoppingApp.model.productHomepage.TopProductsItem
import com.shop.shoppingApp.model.products.ProductResponse
import com.shop.shoppingApp.util.SharedPref
import com.shop.shoppingApp.util.hide
import com.shop.shoppingApp.util.show
import com.shop.shoppingApp.util.whiteStatusBar
import kotlinx.android.synthetic.main.actionbar_primary.*
import kotlinx.android.synthetic.main.actionbar_search.*
import kotlinx.android.synthetic.main.activity_product.*

class Product : BaseActivtiy() {
    var page = 0
    var loading = true
    var visibleItemCount = 0
    var totalItemCount = 0
    var pastVisiblesItems = 0
    var key = ""
    lateinit var lm: GridLayoutManager
    var mainlist = mutableListOf<TopProductsItem>()
    lateinit var adpter: ProductAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        whiteStatusBar()
        setContentView(R.layout.activity_product)
        back.setOnClickListener {
            supportFinishAfterTransition()
            // onBackPressed()
        }
        back_search.setOnClickListener { onBackPressed() }
        adpter = ProductAdapter(mainlist)
        lm = GridLayoutManager(c, 2, GridLayoutManager.VERTICAL, false)
        rv.apply {
            layoutManager = lm
            adapter = adpter
        }
        if (intent.getStringExtra("isSearch").toString() == "yes") {
            ll_search.show()
            ll_title.hide()
        } else getProduct()

        img_search.setOnClickListener {
            if (key.isNotEmpty()) {
                searchProduct()
            }
        }
        search_list_et.doAfterTextChanged {
            key = it.toString()
        }

        search_list_et.setOnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                if (key.isNotEmpty()) {
                    searchProduct()
                }
            }
            true
        }
        action_bar_title.text = intent.getStringExtra("cat")
        rv.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (dy > 0 && mainlist.size >= 20) {
                    visibleItemCount = lm.childCount
                    totalItemCount = lm.itemCount
                    pastVisiblesItems =
                        lm.findFirstVisibleItemPosition()
                    Log.d("TAG", "totalItemCount: $totalItemCount")
                    if (loading) {
                        if (visibleItemCount + pastVisiblesItems >= totalItemCount) {
                            loading = false
                            page++
                            getProduct()
                        }
                    }
                }
            }
        })
    }

    private fun getProduct() {
        callApi(
            RetrofitClient().getProduct(
                SharedPref.getUSERTOKEN(),
                intent.getStringExtra("cat").toString(), page.toString()
            )
        ) {
            response(it)
        }
    }

    private fun searchProduct() {
        callApi(
            RetrofitClient().searchProduct(
                SharedPref.getUSERTOKEN(),
                key
            )
        ) {
            response(it)
        }
    }

    private fun response(it: Any) {

        when (it) {
            is ProductResponse -> {
                if (!it.error) {
                    if (page == 0) mainlist.clear()
                    loading = true
                    mainlist.addAll(it.data)
                    adpter.notifyDataSetChanged()
                }
            }
        }
    }


}