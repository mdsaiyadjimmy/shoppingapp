package com.shop.shoppingApp.main.fragment.home.adp

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.github.demono.adapter.InfinitePagerAdapter
import com.shop.shoppingApp.R
import com.shop.shoppingApp.ShoppingApp.Companion.appCtx
import com.shop.shoppingApp.model.productHomepage.BannersItem
import com.shop.shoppingApp.util.setImg
import kotlinx.android.synthetic.main.item_banner.view.*

class ImageAutoScrollPagerHomeAdp(val list: MutableList<BannersItem>) :
    InfinitePagerAdapter() {
    override fun getItemCount(): Int {
        return list.size
    }

    override fun getItemView(position: Int, p1: View?, viewGroup: ViewGroup?): View {

        var v = LayoutInflater.from(appCtx).inflate(
            R.layout.item_banner,
            viewGroup, false
        )
        v.setOnClickListener {
            //  Util.imgDialog(context,list[position])
//            val opt= ActivityOptionsCompat.
//            makeSceneTransitionAnimation((context as Activity),v.item_image_img,"img")
//
//            context.startActivity(
//                Intent(context, ImgZoomIn::class.java).putExtra(
//                    "img",
//                    list[position].image
//                ),opt.toBundle()
//            )
            //(context as Activity).overridePendingTransition(R.anim.zoom_in, R.anim.zoom_out)
        }
        v.item_image_img.setImg(list[position].url)
        return v
    }

}