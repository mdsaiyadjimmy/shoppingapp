package com.shop.shoppingApp.main.fragment.home.adp


import android.app.Activity
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityOptionsCompat
import androidx.recyclerview.widget.RecyclerView
import com.shop.shoppingApp.R
import com.shop.shoppingApp.main.activity.product.Product
import com.shop.shoppingApp.model.productHomepage.CategoriesItem
import com.shop.shoppingApp.util.capsWord
import com.shop.shoppingApp.util.pushDownANim.PushDownAnim
import com.shop.shoppingApp.util.setImg
import kotlinx.android.synthetic.main.item_category.view.*
import kotlinx.android.synthetic.main.item_product.view.*


class CatAdp( var mainlist: MutableList<CategoriesItem>) :
    RecyclerView.Adapter<CatAdp.MyViewHolder>() {
lateinit var ctx:Context
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): MyViewHolder {
        ctx=p0.context
        return MyViewHolder(
            LayoutInflater.from(p0.context).inflate(
                R.layout.item_category,
                p0,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return mainlist.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
       holder.itemView.apply {
           mainlist[position].let {
               item_cat_img.setImg(it.img)
               item_cat_title.text = it.name.toString().capsWord()
               PushDownAnim.setPushDownAnimTo(row).setOnClickListener {_->
//                   val opt = ActivityOptionsCompat.makeSceneTransitionAnimation(
//                       (ctx as Activity),
//                       item_cat_title,"title"
//                   )
                   ctx.startActivity(
                       Intent(ctx, Product::class.java)
                           .putExtra("cat",it.name))
               }
           }

       }


    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }
}