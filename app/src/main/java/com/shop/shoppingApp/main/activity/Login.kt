package com.shop.shoppingApp.main.activity

import android.app.Activity
import android.content.Intent
import android.content.IntentFilter
import android.net.Uri
import android.os.Bundle
import android.os.CountDownTimer
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.view.isVisible
import com.github.dhaval2404.imagepicker.ImagePicker
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.credentials.Credential
import com.google.android.gms.auth.api.credentials.HintRequest
import com.google.android.gms.auth.api.phone.SmsRetriever
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.shop.shoppingApp.R
import com.shop.shoppingApp.api.RetrofitClient
import com.shop.shoppingApp.broadcast.MySMSBroadcastReceiver
import com.shop.shoppingApp.main.BaseActivtiy
import com.shop.shoppingApp.model.commonModel.CommonResponse
import com.shop.shoppingApp.model.loginModel.LoginSignupResponse
import com.shop.shoppingApp.util.*
import kotlinx.android.synthetic.main.actionbar_primary.*
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_signup.*
import kotlinx.android.synthetic.main.otp_view.*

class Login : BaseActivtiy(), GoogleApiClient.ConnectionCallbacks,
    GoogleApiClient.OnConnectionFailedListener {
    var sfname = ""
    var smob = ""
    var scity = ""
    var semail = ""
    var spinCode = ""
    var sflate = ""
    var saddress = ""
    var profileUri: Uri? = null
    var profileUrl = "null"
    var canreq = true
    var counter = 60
    lateinit var googleApiClient: GoogleApiClient

    var cd: CountDownTimer? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        whiteStatusBar()
        setContentView(R.layout.activity_login)

        registerBroadcast()
        googleApiClient = GoogleApiClient.Builder(c)
            .addApi(Auth.CREDENTIALS_API)
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this)
            .build()
        auth_login.setOnClickListener {
            checkcredntial()
        }

        next.setOnClickListener {
            startActivity(Intent(c, MainActivity::class.java).putExtra("main", true))
            SharedPref.setSKIP(true)
        }
        resend_otp.setOnClickListener {
            counter = 60
            recmsg()
            cd?.cancel()
            cd?.onFinish()
            cd = null
            callApi(RetrofitClient().sendOtp(smob)) {
                response(it)
            }
        }

        profile_pic.setOnClickListener { takePicture() }
        action_bar_title.text = "Login/Register"
        next.text = "Skip"



        back.setOnClickListener { onBackPressed() }
        signup_create_account.setOnClickListener { checkRegcredntial() }

        login_mobile.setOnClickListener {
            editable()
            if (canreq) requestHint()
        }


        otp_view.setOtpCompletionListener {

            checkLogincred(it)
        }

    }

    override fun onPause() {
        super.onPause()
        unregisterReceiver(broadcastReceiver)
    }

    override fun onResume() {
        super.onResume()
        registerBroadcast()
        dismissDialog()
    }

    var broadcastReceiver = MySMSBroadcastReceiver()
    private fun registerBroadcast() {
        val otpListener = object : MySMSBroadcastReceiver.OTPReceiveListener {
            override fun onOTPReceived(otp: String) {
                otp_view.setText(otp)
            }

            override fun onOTPTimeOut() {
                toast("timeout")
            }

        }

        broadcastReceiver.initOTPListener(otpListener)

        registerReceiver(
            broadcastReceiver,
            IntentFilter(SmsRetriever.SMS_RETRIEVED_ACTION)
        )

    }

    fun recmsg() {
        var client = SmsRetriever.getClient(this)
        var task = client.startSmsRetriever()


        task.addOnSuccessListener {
            // Util.toast(c, "onSuccess")
        }
        task.addOnFailureListener { }
    }

    // Construct a request for phone numbers and show the picker
    private fun requestHint() {
        canreq = false
        googleApiClient.connect()
        val hintRequest = HintRequest.Builder()
            .setPhoneNumberIdentifierSupported(true)
            .build()
        val intent = Auth.CredentialsApi.getHintPickerIntent(
            googleApiClient, hintRequest
        )
        startIntentSenderForResult(
            intent.intentSender,
            RESOLVE_HINT, null, 0, 0, 0
        )

    }

    fun checkLogincred(otp: String) {
        val token = SharedPref.getTOKEN().toString()
        callApi(RetrofitClient().login(smob, otp, token)) {
            response(it)
        }
    }

    private fun response(it: Any) {
        when (it) {
            is LoginSignupResponse -> {

                if (!it.error!!) {
                    it.data?.run {

                        SharedPref.setUSERTOKEN(token.toString())
                        SharedPref.setSKIP(false)
                        SharedPref.setUSER_NAME(name.toString())
                        SharedPref.setUSER_ID(id.toString())
                        SharedPref.setUSER_EMAIL(email.toString())
                        SharedPref.setUSER_MOB(mobile.toString())
                        SharedPref.setUSER_PIC(profilePic.toString())
                        SharedPref.setADDRESS_CITY(city.toString())
                        SharedPref.setADDRESS_ZIPCODE(postcode.toString())
                        SharedPref.setADDRESS_HOUSE(flatNo.toString())
                        SharedPref.setADDRESS_ADDRESS(address.toString())
                        name?.let { signup_full_name.setText(name.toString()) }
                        email?.let { signup_email.setText(email.toString()) }
                        flatNo?.let { signup_flat.setText(flatNo.toString()) }
                        postcode?.let { signup_pin.setText(postcode.toString()) }
                        city?.let { signup_city.setText(city.toString()) }
                        address?.let { signup_address.setText(address.toString()) }
                        profileUrl == profilePic
                        profile_pic.setImgProfile(profilePic)
                        ll_number.hide()
                        ll_otp.hide()
                        ll_logo.hide()
                        ll_signup.show()
                        ll_signup.hideSoftKeyboard()
                        action_bar_title.text = "Register"
                        cd?.cancel()

                    }
                } else toast(it.msg)

            }
            is CommonResponse -> {
                if (!it.error) {
                    action_bar_title.text = "OTP"
                    ll_number.hide()
                    ll_otp.show()
                    otp_view.setText("")
                    verification_code_on.text =
                        "We have sent you a 4 digit verification code on\n$smob"
                    cd?.let { it.start() }
                        ?: run {
                            initTimer()
                        }
                    resend_otp.hide()

                } else {
                    toast(it.message)
                }
            }
        }
    }

    private fun initTimer() {

        cd = object : CountDownTimer(60000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                counttime.text = "00:$counter"
                counttime.text = java.lang.String.valueOf(counter)
                counter--
            }

            override fun onFinish() {
                counttime.text = "Time Out"
                cd = null
                resend_otp.show()

            }
        }
        cd?.start()
    }

    private fun responseReg(it: Any) {
        when (it) {
            is LoginSignupResponse -> {
                toast(it.msg)
                if (!it.error!!) {
                    it.data?.run {
                        SharedPref.run {
                            setUSERTOKEN(token.toString())
                            setSKIP(false)
                            setUSER_NAME(name.toString())
                            setUSER_NAME(name.toString())
                            setUSER_ID(id.toString())
                            setUSER_EMAIL(email.toString())
                            setUSER_MOB(mobile.toString())
                            setUSER_PIC(profilePic.toString())
                            setADDRESS_CITY(city.toString())
                            setADDRESS_ZIPCODE(postcode.toString())
                            setADDRESS_HOUSE(address.toString())
                            setADDRESS_ADDRESS(address.toString())
                        }
                        finishAffinity()
                        startActivity(
                            Intent(c, MainActivity::class.java).putExtra(
                                "main",
                                true
                            )
                        )

                    }
                }

            }
            is CommonResponse -> {
                if (!it.error!!) {
                    finishAffinity()
                    startActivity(
                        Intent(c, MainActivity::class.java).putExtra(
                            "main",
                            true
                        )
                    )
                }
            }

        }
    }

    fun editable() {
        login_mobile.isClickable = true
        login_mobile.isFocusable = true
        login_mobile.isFocusableInTouchMode = true
        login_mobile.requestFocus()
    }

    override fun onBackPressed() {
        if (ll_otp.isVisible) {
            ll_otp.hide()
            ll_number.show()
            action_bar_title.text = "Login/Register"
            cd?.cancel()
            cd?.onFinish()
            counter = 60
        } else if (ll_signup.isVisible) {
            action_bar_title.text = "Login/Register"
            ll_otp.hide()
            ll_signup.hide()
            ll_logo.show()
            ll_number.show()
            cd?.cancel()
            cd?.onFinish()
            counter = 60
        } else {
            super.onBackPressed()
        }
    }

    private fun checkcredntial() {
        smob = login_mobile.text.toString()
        if (!smob.isValidPhone()) {
            login_mobile.error = "Invalid Mobile"
            login_mobile.requestFocus()
        } else {

//            recmsg()
            callApi(RetrofitClient().sendOtp(smob)) { response(it) }
        }
    }

    private fun checkRegcredntial() {
        sfname = signup_full_name.text.toString()
        semail = signup_email.text.toString()
        sflate = signup_flat.text.toString()
        scity = signup_city.text.toString()
        saddress = signup_address.text.toString()
        spinCode = signup_pin.text.toString()
        if (profileUri == null && profileUrl.isNullOrEmpty()) toast("Select Profile Image")
        else if (signup_full_name.check()) return
        else if (signup_flat.check(3)) return
        else if (signup_city.check(5)) return
        else if (signup_address.check(10)) return
        else if (signup_pin.check(6)) return
        else if (!semail.isValidEmail()) {
            signup_email.error = "Invalid Email"
            signup_email.requestFocus()
        } else {

//            recmsg()
            callApi(
                RetrofitClient().updateProfile(
                    SharedPref.getUSERTOKEN().toString(),
                    sfname.getReqBody(),
                    semail.getReqBody(),
                    scity.getReqBody(),
                    sflate.getReqBody(),
                    saddress.getReqBody(),
                    spinCode.getReqBody(),
                    profileUri.getReqBody("profile_pic")
                )
            ) { responseReg(it) }
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == 9001) {

            } else if (requestCode == RESOLVE_HINT) {
                val credential: Credential? = data!!.getParcelableExtra(Credential.EXTRA_KEY)
                //  Util.toast(c,credential.id)
                login_mobile.setText(credential?.id?.substring(3))
                // credential.getId();  <-- will need to process phone number string
            }

        }
    }

    override fun onConnected(p0: Bundle?) {
        // Util.toast(c,p0.toString())

    }

    override fun onConnectionSuspended(p0: Int) {
    }

    override fun onConnectionFailed(p0: ConnectionResult) {
        toast(p0.errorMessage.toString())

    }

    private fun takePicture() {
        next.hideSoftKeyboard()
        ImagePicker.with(this)
            //.cropSquare()
            .compress(512)         //Final image size will be less than 1 MB(Optional)
            .galleryMimeTypes(arrayOf("image/png", "image/jpeg", "image/jpg"))
            .maxResultSize(
                540,
                1080
            )  //Final image resolution will be less than 1080 x 1080(Optional)
            .createIntent { intent ->
                showDialog()
                startForPicResult.launch(intent)
            }
    }

    private val startForPicResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
            val resultCode = result.resultCode
            val data = result.data
            log(msg = "Result--->")
            when (resultCode) {
                Activity.RESULT_OK -> {
                    profileUri = data?.data!!

                    data?.data?.let { profile_pic.setImg(it, 100) }
                    log(msg = "Result--- after set img>")
                }
            }
        }
}