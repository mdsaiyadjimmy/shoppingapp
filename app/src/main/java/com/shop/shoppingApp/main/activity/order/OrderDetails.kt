package com.shop.shoppingApp.main.activity.order

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.shop.shoppingApp.R
import com.shop.shoppingApp.api.RetrofitClient
import com.shop.shoppingApp.main.BaseActivtiy
import com.shop.shoppingApp.model.orders.OrderDetailResponse
import com.shop.shoppingApp.model.orders.OrderItem
import com.shop.shoppingApp.model.orders.OrderItems
import com.shop.shoppingApp.util.*
import kotlinx.android.synthetic.main.actionbar_primary.*
import kotlinx.android.synthetic.main.order_details.*


class OrderDetails : BaseActivtiy() {
    lateinit var data: OrderItems
    var mainlist = mutableListOf<OrderItem>()

    lateinit var itemadpter: OrderItemAdp

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.order_details)

        next.hide()
        action_bar_title.text = "Order Detail"
        data = Gson().fromJson(intent.getStringExtra("data").toString(), OrderItems::class.java)
        order_id.text = "Order ID - " + data.id
        setData()
        back.setOnClickListener {
            finish()
        }
        rv.apply {
            layoutManager = LinearLayoutManager(c)
            itemadpter = OrderItemAdp(mainlist)
            adapter = itemadpter
        }
    }

    private fun setData() {
        ll_main.show()
        data?.apply {

            order_address.text = "$flatNo $address\n$city,$state$pincode "
            order_name.text = SharedPref.getUSER_NAME().toString().capsWord()
            order_phone.text = SharedPref.getUSER_MOB()
            order_mrp.text = subtotal.setAmount()
            order_total.text = payableAmt.setAmount()
//                        order_selling_price.text = price.setAmount()
            order_total_paybale.text = payableAmt.setAmount()
            if (isCod == 1) order_payment_type.text = "COD"
            else order_payment_type.text = "Online"
            if (isPaid == 1) order_payment_status.text = "Payment : Done"
            else order_payment_status.text = "Payment : Pending"
            order_additional_fee.text = "00".setAmount()
            order_shipping_fee.text = deliveryCharges.setAmount()
            // order_phone.text = .
            // order_rating.rating = .orderRating?.toFloat()

            order_status.apply {

                when (status) {
                    "pending" -> {
                        text = "Order Pending"
                        setTextClr(R.color.brown)
                        setBgClr(R.color.brown_light)
                    }
                    "accepted" -> {
                        text = "Order Accepted"
                        setTextClr(R.color.blue)
                        setBgClr(R.color.blue_light)
                    }
                    "delivered" -> {
                        text = "Order Delivered"
                        setTextClr(R.color.green)
                        setBgClr(R.color.green_light)
                    }
                    "cancelled" -> {
                        text = "Order Cancelled"
                        setTextClr(R.color.red)
                        setBgClr(R.color.pink_light)
                    }

                }
            }
        }
    }


    override fun onResume() {
        super.onResume()
        getDetail()
    }

    private fun getDetail() {
        callApi(
            RetrofitClient().getOrderDetails(
                data.id.toString()
            )
        ) {
            if (it is OrderDetailResponse) {
                if (!it.error!!) {
                    mainlist.clear()
//                        timeline.addAll(res.orderTimeline)
                    it.data?.let { it1 -> mainlist.addAll(it1.orderItems) }
                    itemadpter.notifyDataSetChanged()
                    if (mainlist.isNotEmpty()) item_card.show()

                }
            }
        }
    }
}
