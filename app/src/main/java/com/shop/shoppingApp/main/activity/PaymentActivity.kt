package com.shop.shoppingApp.main.activity

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.CompoundButton
import android.widget.RadioButton
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import com.razorpay.Checkout
import com.razorpay.PaymentResultListener
import com.shop.shoppingApp.R
import com.shop.shoppingApp.ShoppingApp.Companion.isFromOrder
import com.shop.shoppingApp.ShoppingApp.Companion.transactionId
import com.shop.shoppingApp.ShoppingApp.Companion.txnCancel
import com.shop.shoppingApp.api.RetrofitClient
import com.shop.shoppingApp.main.BaseActivtiy
import com.shop.shoppingApp.model.cart.CartTotalResponse
import com.shop.shoppingApp.model.commonModel.CommonResponse
import com.shop.shoppingApp.model.createOrder.CreateOrderResponse
import com.shop.shoppingApp.model.loginModel.LoginSignupResponse
import com.shop.shoppingApp.model.settings.SettingsResponse
import com.shop.shoppingApp.util.*
import kotlinx.android.synthetic.main.actionbar_primary.*
import kotlinx.android.synthetic.main.payment.*
import org.json.JSONObject
import kotlin.math.roundToInt

class PaymentActivity : BaseActivtiy(), PaymentResultListener {
    // $keyId='rzp_test_CQ8yJMSk6xb5F1';
//// $keySecret='ZfxEzoPncsbC3ZGN7knPNnHN'
    var applyWallet = "0"
    var couponCode = ""
    var total = "0.0"
    var minOrder = 0.0

    //    var transactionId = ""
    var paymentMethod = ""
    var orderNo = ""
    var loading = true

    //    var txnCancel = false
    lateinit var layoutManager: LinearLayoutManager
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.payment)
        //whiteStatusbar(c)
        action_bar_title.text = "Payment"
        next.hide()
        payment_continue.isClickable = false
        back.setOnClickListener {
            finish()
        }

        change_add.setOnClickListener {
            transactionId = ""
            startActivity(Intent(c, EditProfile::class.java).putExtra("isEdit", true))
        }

        payment_continue.setOnClickListener {
            transactionId = ""
            paymentMethod =
                (findViewById<View>(payment_rg_option.checkedRadioButtonId) as RadioButton).text.toString()
            if (paymentMethod.isNotEmpty() && paymentMethod == "Online") {
                buyFxn()//  if (!orderNo.isNullOrEmpty()) startPayment()
            } else buyFxn()
        }

        cb_apply_wallet.setOnCheckedChangeListener { p0, p1 ->
            if (p0!!.isPressed) {
                applyWallet = if (p1) "1"
                else "0"
                getCartTotal()
            }
        }
        btn_apply_coupon.setOnClickListener {
            if (et_coupon.check(4)) return@setOnClickListener
            else {
                couponCode = et_coupon.text.toString()
                getCartTotal()
            }
        }
        tv_apply_coupon.setOnClickListener {
            if (ll_apply_coupon.isVisible) {
                ll_apply_coupon.hide()
                img_arrow.animate().rotationBy(180f)
            } else {
                img_arrow.animate().rotationBy(-180f)
                ll_apply_coupon.show()
            }
        }


    }

    override fun onResume() {
        super.onResume()
        SharedPref.apply {
            payment_address_type.text = "Home"
            // checkPin(getADDRESS_ZIPCODE().toString())
            payment_address_house_no.text =
                getADDRESS_HOUSE() + " " + getADDRESS_CITY() + " " + getADDRESS_STATE() + "\n" + getADDRESS_ZIPCODE()
            payment_address_mob.text = getUSER_MOB()
            payment_address_name.text = getUSER_NAME()
            // setAmount()
        }
        log(msg = "onResume$transactionId")
        if (transactionId.isEmpty() && !txnCancel) {
            Log.d("TAG", "onResume: in $transactionId")
            orderNo = ""
            getCartTotal()
            getProfile()
        }
    }

    fun checkPin(pin: String) {
        callApi(RetrofitClient().pinCheck(SharedPref.getUSERTOKEN(), pin)) {
            if (it is CommonResponse) {
                if (!it.error) {
                    ll_note.show()
                    payment_note.text = it.message
                }
            }
        }
    }

    fun getProfile() {
        callApi(RetrofitClient().getProfile(SharedPref.getUSERTOKEN())) {
            if (it is LoginSignupResponse) {
                if (!it.error!!) {
                    ll_note.show()
                    wallet_bal.text = it.data?.wallet.setAmount()
                    if (it.data?.wallet.getAmount() > 0) ll_cb_wallet.show()
                    else ll_cb_wallet.hide()
                }
            }
        }
    }


    fun getCartTotal() {
        callApi(
            RetrofitClient().getCartTotal(
                couponCode,
                applyWallet
            )
        ) {
            response(it)
        }

        callApi(
            RetrofitClient().getSettings()
        ) {
            response(it)
        }
    }


    private fun buyFxn() {
        callApi(
            RetrofitClient().createOrder(
                couponCode,
                applyWallet,
               // transactionId,
                paymentMethod.lowercase(),
                //orderNo,
            )
        ) {
            response(it)
        }

    }

    private fun updatePayment() {
        callApi(
            RetrofitClient().updatePayment(
                transactionId,
                orderNo,
            )
        ) {
            response(it)
        }

    }


    private fun response(it: Any) {

        when (it) {
            is CartTotalResponse -> {
                if (!it.error) {
                    it.data?.run {
                        if (couponCode.isNotEmpty()) toast(it.msg)
                        payment_main.show()
                        payment_sub_total.text = subTotal.setAmount()
                        payment_total.text = netPayable.setAmount()
                        total = netPayable.getAmount().toString()
                        orderNo = nextOrderNo.toString()
                        txnCancel = false
                        payment_delivery_charges.text = deliveryCharges.setAmount()
                        checkMinOrder()
                        if (totalSavings.getAmount() > 0) {
                            payment_saving.show()
                            payment_discount.text = "-" + totalSavings.setAmount()
                            payment_saving.text =
                                "You are saving ${totalSavings.setAmount()} on this order!"
                        } else payment_saving.hide()
                        if (discount.getAmount() > 0) {
                            payment_coupon_discount.text = "-" + discount.setAmount()
                            payment_coupon_discount.show()
                        } else payment_coupon_discount.hide()
                        if (deliveryCharges.getAmount() > 0) {
                            ll_note.show()
                            payment_note.text =
                                "You need to pay extra ${deliveryCharges.setAmount()} delivery charge!"
                        } else ll_note.hide()
                        payment_grand_total.text = netPayable.setAmount()
                    }
                } else toast(it.msg)

            }
            is CreateOrderResponse -> {
                if (!it.error!!) {
                    if (paymentMethod.isNotEmpty() && paymentMethod == "Online") {
                        it?.data?.run {
                            orderNo=id.toString()
                            total = payableAmt.getAmount().toString()
                            startPayment()
                        }

                    } else{
                    successDialog(orderId = it?.data?.id.toString()) {
                        isFromOrder = true
                        finishAffinity()
                        startActivity(MainActivity())
                    }}

                } else {
                    toast(it.msg)
                }
            } is CommonResponse -> {
                if (!it.error!!) {

                    successDialog(orderId = orderNo) {
                        isFromOrder = true
                        finishAffinity()
                        startActivity(MainActivity())
                    }

                } else {
                    toast(it.message)
                }
            }
            is SettingsResponse -> {
                if (!it.error!!) {
                    minOrder = it.data?.minimumOrder.getAmount()
                    checkMinOrder()

                } else {
                    toast(it.msg)
                }
            }

        }
    }

    private fun checkMinOrder() {
        if (total.getAmount() < minOrder) {
            cart_ll_min_order.show()
            cart_ll_bottom.hide()
        } else {
            cart_ll_bottom.show()
            cart_ll_min_order.hide()
        }
        minimum_order.text = "Minimum order amount should be at least Rs $minOrder"

    }


    fun startPayment() {
        txnCancel = false
        val co = Checkout()
        try {
            val options = JSONObject()
            options.put("image", resources.getDrawable(R.mipmap.ic_launcher))
            options.put("currency", "INR")
            var finalPrice = total.toDouble().roundToInt()
            log("TAG", "bbb:   $finalPrice")
            finalPrice *= 100
            options.put("amount", finalPrice)
            options.put("id", orderNo)
            val preFill = JSONObject()
            preFill.put("email", SharedPref.getUSER_EMAIL())
            preFill.put("contact", SharedPref.getUSER_MOB())
            options.put("prefill", preFill)
            //secret key bTqQogPf3hMJMZOeVs4LzDDR
            //co.setKeyID("rzp_test_CQ8yJMSk6xb5F1")
            co.setKeyID("rzp_live_Cm2ZKJqpgE5bZH")
            co.setImage(R.drawable.logo)
            co.open(c, options)

        } catch (e: Exception) {
            toast("Error in payment: " + e.message)
            e.printStackTrace()
        }
    }

    override fun onPaymentError(errorCode: Int, response: String?) {
        toast("Error in payment: $response")
        txnCancel = true
    }

    override fun onPaymentSuccess(razorpayPaymentID: String?) {
        log(msg = "onPayment" + transactionId)
        paymentMethod=""
        transactionId = razorpayPaymentID.toString()
        // buyFxn()
        updatePayment()
    }
}
