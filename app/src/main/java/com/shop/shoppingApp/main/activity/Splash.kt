package com.shop.shoppingApp.main.activity

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.app.ActivityOptionsCompat
import com.google.firebase.messaging.FirebaseMessaging
import com.shop.shoppingApp.R
import com.shop.shoppingApp.util.SharedPref
import com.shop.shoppingApp.util.Util
import com.shop.shoppingApp.util.noStatusNoNav
import kotlinx.android.synthetic.main.activity_splash.*
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.FirebaseApp
import com.shop.shoppingApp.ShoppingApp
import com.shop.shoppingApp.util.log

class Splash : AppCompatActivity() {
    private val animationStarted = false
    var c = this@Splash
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        //  AppSignatureHelper(c).appSignatures
//        FirebaseApp.initializeApp(ShoppingApp.appCtx)
        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) {
                log(msg = "Fetching FCM registration token failed" + task.exception)
                return@OnCompleteListener
            }

            val token = task.result
            log(msg = token)
            SharedPref.setTOKEN(token)

        })

        Handler(Looper.myLooper()!!).postDelayed({
            if (SharedPref.getUSER_ID().toString().isEmpty()) {
                val opt = ActivityOptionsCompat.makeSceneTransitionAnimation(
                    c,
                    splash_img,
                    "logo"
                )
                Log.d("TAG", "splash if")
                Log.d("TAG", "VID : " + SharedPref.getUSER_ID())
                startActivity(Intent(this@Splash, Login::class.java), opt.toBundle())
                finish()
            } else {


                startActivity(Intent(c, MainActivity::class.java))
                finish()
            }
            //
        }, 2000)

    }


    override fun onWindowFocusChanged(hasFocus: Boolean) {
        Log.d("TAG", "onWindowFocusChanged")
        super.onWindowFocusChanged(hasFocus)
        if (!hasFocus || animationStarted) {
            return
        }
        //  animate()
        if (hasFocus) {
            noStatusNoNav()
        }
    }


}
