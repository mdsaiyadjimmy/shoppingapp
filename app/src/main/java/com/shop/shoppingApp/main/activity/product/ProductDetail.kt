package com.shop.shoppingApp.main.activity.product

import android.graphics.Paint
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import androidx.recyclerview.widget.GridLayoutManager
import com.google.gson.Gson
import com.shop.shoppingApp.R
import com.shop.shoppingApp.api.RetrofitClient
import com.shop.shoppingApp.main.BaseActivtiy
import com.shop.shoppingApp.main.activity.PaymentActivity
import com.shop.shoppingApp.main.fragment.home.adp.ProductAdapter
import com.shop.shoppingApp.model.cart.AddToCartResponse
import com.shop.shoppingApp.model.commonModel.CommonResponse
import com.shop.shoppingApp.model.productHomepage.TopProductsItem
import com.shop.shoppingApp.model.products.ProductResponse
import com.shop.shoppingApp.util.*
import kotlinx.android.synthetic.main.actionbar_primary.*
import kotlinx.android.synthetic.main.activity_product.*
import kotlinx.android.synthetic.main.activity_product_detail.*
import kotlinx.android.synthetic.main.item_product.view.*

class ProductDetail : BaseActivtiy() {
    var page = 0
    var loading = true
    var data = TopProductsItem()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        whiteStatusBar()
        setContentView(R.layout.activity_product_detail)
        back.setOnClickListener { supportFinishAfterTransition() }

        data = Gson().fromJson(intent.getStringExtra("data"), TopProductsItem::class.java)
        setData()
        addToCart.setOnClickListener { _ ->
            callApi(
                RetrofitClient().addToCart(
                    SharedPref.getUSERTOKEN(),
                    data.id.toString(),
                    data.variantId.getList()[data.pos], "1"
                )
            ) { res ->
                if (res is AddToCartResponse) {
                    if (!res.error) {
                        toast("Item Added to Cart")
                    }
                }
            }
        }
        buy_now.setOnClickListener { _ ->
            callApi(
                RetrofitClient().addToCart(
                    SharedPref.getUSERTOKEN(),
                    data.id.toString(),
                    data.variantId.getList()[data.pos], "1"
                )
            ) { res ->
                if (res is AddToCartResponse) {
                    if (!res.error) {
                        startActivity(PaymentActivity())
                    }
                }
            }

        }
    }

    private fun setData() {
        data.let {
            action_bar_title.text = it.name?.capsWord()
            product_img.setImg(it.img)
            ratingbar.rating = 5.0f
            product_title.text = it.name?.capsWord()
            if (it.description.isNullOrEmpty()) {
                product_desc.hide()
                product_abt.hide()
            } else product_desc.text = it.description

            product_brand.text = "By " + it.brand?.capsWord()
            this.setPrice(it, 0)
            sp_unit.adapter = getadp(it.sizes.getList())
            sp_unit.setSelection(0, false)
            sp_unit.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    p0: AdapterView<*>?,
                    p1: View?,
                    pos: Int,
                    p3: Long
                ) {
                    log(msg = "Selected pos $pos")
                    setPrice(it, pos)
                }

                override fun onNothingSelected(p0: AdapterView<*>?) {
                }

            }
            addToCart.setOnClickListener { _ ->
                toast("Item Added to cart!")

            }
        }
    }

    private fun setPrice(item: TopProductsItem, pos: Int) {
        item.pos = pos
        var mrp = item.mrps.getList()[pos]
        var dPrice = item.sps.getList()[pos]
        product_price.text = dPrice.setAmount()
        product_disc_price.apply {
            text = mrp.setAmount()
            paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
            if (mrp == dPrice) hide()
            else show()
        }
        var off = getpercent(mrp.getAmount(), dPrice.getAmount())
        if (off > 0) {
            product_offer.text = "$off% Off"
            product_offer.show()
        } else product_offer.hide()

    }

    private fun getProduct() {
        callApi(
            RetrofitClient().getProduct(
                SharedPref.getUSERTOKEN(),
                intent.getStringExtra("cat").toString(), page.toString()
            )
        ) {
            if (it is ProductResponse) {
                if (!it.error) {

                }
            }
        }
    }


}