package com.shop.shoppingApp.main.fragment.profile

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.shop.shoppingApp.R
import com.shop.shoppingApp.api.RetrofitClient
import com.shop.shoppingApp.main.BaseFragment
import com.shop.shoppingApp.main.activity.EditProfile
import com.shop.shoppingApp.main.activity.ReferNEarn
import com.shop.shoppingApp.main.activity.Splash
import com.shop.shoppingApp.main.activity.WebView
import com.shop.shoppingApp.main.activity.wallet.Wallet
import com.shop.shoppingApp.model.loginModel.LoginSignupResponse
import com.shop.shoppingApp.util.SharedPref
import com.shop.shoppingApp.util.optionDialog
import com.shop.shoppingApp.util.setAmount
import com.shop.shoppingApp.util.setImgProfile
import kotlinx.android.synthetic.main.activity_signup.*
import kotlinx.android.synthetic.main.frag_profile.*
import kotlinx.android.synthetic.main.wallet.*

class ProfileFrag : BaseFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.frag_profile, container, false)

    }

    override fun onResume() {
        super.onResume()
        SharedPref.run {
            profile_name.text = getUSER_NAME().toString()
            profile_number.text = getUSER_MOB().toString()
            profile_img.setImgProfile(getUSER_PIC())
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        frag_profile_notification.setOnClickListener {
            //  startActivity(Intent(c, Notification::class.java))
        }
        getProfile()


        frag_profile_setting.setOnClickListener {
            c.optionDialog("Logout") {
                if (it) {
                    SharedPref.clearSession()
                    activity?.finishAffinity()
                    startActivity(Splash())
                }
            }
        }

        frag_profile_edit_profile.setOnClickListener {
            startActivity(Intent(c, EditProfile::class.java).putExtra("isEdit",false))
        }

        frag_profile_wallet.setOnClickListener {
            startActivity(Wallet())
        }

        refer.setOnClickListener {
            startActivity(ReferNEarn())
        }
        aboutUs.setOnClickListener {
            startActivity(Intent(c, WebView::class.java)
                .putExtra("from", "About Us")
                .putExtra("to", "https://zayadryfruits.com/about-us.php")
            )
        }
        privacyPolicy.setOnClickListener {
            startActivity(Intent(c, WebView::class.java)
                .putExtra("from", "Privacy Policy")
                .putExtra("to", "https://zayadryfruits.com/privacy-policy.php")
            )
        }
        termsCondition.setOnClickListener {
            startActivity(Intent(c, WebView::class.java)
                .putExtra("from", "Terms & Condition")
                .putExtra("to", "https://zayadryfruits.com/terms-and-conditions.php")
            )
        }

        support.setOnClickListener {
           // startActivity(Intent(c, WebView::class.java).putExtra("from", "Support"))
            val uri: Uri =
                Uri.parse("https://api.whatsapp.com/send?phone=" + "$+919958344409 "+ "&text=" + "I need help!")

            val sendIntent = Intent(Intent.ACTION_VIEW, uri)
            startActivity(sendIntent)
        }


    }
    fun getProfile() {
        callApi(RetrofitClient().getProfile(SharedPref.getUSERTOKEN())) {
            if (it is LoginSignupResponse) {
                if (!it.error!!) {
                    // ll_note.show()
                    profile_bal.text = it.data?.wallet.setAmount()
                    // if (it.data?.wallet.getAmount() > 0) ll_cb_wallet.show()
                    // else ll_cb_wallet.hide()
                }
            }
        }
    }

}