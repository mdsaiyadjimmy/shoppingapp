package com.shop.shoppingApp.main.activity.adress


import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.shop.shoppingApp.R
import com.shop.shoppingApp.main.BaseActivtiy
import com.shop.shoppingApp.model.addressModel.DataItem
import com.shop.shoppingApp.util.*
import kotlinx.android.synthetic.main.actionbar_primary.*
import kotlinx.android.synthetic.main.select_address.*

class AddressList : BaseActivtiy() {

    var mainlist = mutableListOf<DataItem>()
    var loading = true
    var selectedId = ""
    var city = ""
    var address = ""
    var postalCode = ""
    var lat = ""
    var lng = ""
    var name = ""
    var cnt = 1
    var type = ""
    var position = 0
    lateinit var layoutManager: LinearLayoutManager
    lateinit var adpter: AddressAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        whiteStatusBar()
        setContentView(R.layout.select_address)
        adpter = AddressAdapter(mainlist) {
            showButton(it)
        }
        layoutManager = LinearLayoutManager(c)
        rv_address.layoutManager = layoutManager
        rv_address.setHasFixedSize(true)
        adpter.hasStableIds()
        rv_address.adapter = adpter

        select_address_add_new_address.setOnClickListener {
            startActivity(Intent(c, AddAddress::class.java).putExtra("edit", false))
        }

        back.setOnClickListener {
            finish()
        }

        select_address.setOnClickListener {
            SharedPref.run {

                setDEL_ADDRESS_ID(mainlist[position].id.toString())
                setDEL_ADDRESS_TYPE(mainlist[position].addressType.toString())
                setDEL_ADDRESS_LAT(mainlist[position].lat.toString())
                setDEL_ADDRESS_LNG(mainlist[position].lng.toString())
                setDEL_ADDRESS_NAME(mainlist[position].firstName.toString())
                finish()
            }
        }
        action_bar_title.text = "Select Delivery Address"

    }


    fun showButton(pos: Int) {
        position = pos

        when (position) {
            -1 -> {
                select_address.animation = Anim().slide_out_bottom
                select_address.hide()
            }
            else -> {
                select_address.show()
                select_address.animation = Anim().slide_in_bottom
            }
        }

    }

    override fun onResume() {
        super.onResume()

    }
}
