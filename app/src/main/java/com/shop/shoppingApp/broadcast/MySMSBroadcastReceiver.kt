package com.shop.shoppingApp.broadcast

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.android.gms.auth.api.phone.SmsRetriever
import com.google.android.gms.common.api.CommonStatusCodes
import com.google.android.gms.common.api.Status

open class MySMSBroadcastReceiver : BroadcastReceiver() {
    private var otpReceiver: OTPReceiveListener? = null
    fun initOTPListener(receiver: OTPReceiveListener) {
        this.otpReceiver = receiver
    }

    override fun onReceive(context: Context, intent: Intent) {
        if (SmsRetriever.SMS_RETRIEVED_ACTION == intent.action) {
            Log.d("TAG", " MySMSBroadcastReceiver here")

            val extras = intent.extras
            val status = extras!![SmsRetriever.EXTRA_STATUS] as Status?
            when (status!!.statusCode) {
                CommonStatusCodes.SUCCESS -> {
                    Log.d("TAG", "CommonStatusCodes MySMSBroadcastReceiver here")
                    // Get SMS message contents
                    val message = extras[SmsRetriever.EXTRA_SMS_MESSAGE] as String?
                    var otp = message!!.split("is ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[1]
                   // var otp: String = extras.get(SmsRetriever.EXTRA_SMS_MESSAGE) as String
                    Log.d("TAG",message+ otp)

                    val myIntent = Intent("otp")
                    myIntent.putExtra("message", otp.trim().substring(0,4))
                    LocalBroadcastManager.getInstance(context).sendBroadcast(myIntent)
                    otpReceiver?.onOTPReceived(otp.trim().substring(0,4))

                }
                CommonStatusCodes.TIMEOUT ->       {
                    otpReceiver?.onOTPTimeOut()
                    Log.d("TAG", "timeout here")
                }              // Waiting for SMS timed out (5 minutes)
                    // Handle the error ...

            }
        }
    }


    interface OTPReceiveListener {

        fun onOTPReceived(otp: String)

        fun onOTPTimeOut()
    }
}