package com.shop.shoppingApp.model.createOrder

import com.google.gson.annotations.SerializedName

data class CreateOrderResponse(

	@field:SerializedName("msg")
	val msg: String? = null,

	@field:SerializedName("data")
	val data: Data? = null,

	@field:SerializedName("error")
	val error: Boolean? = null
)

data class Data(

	@field:SerializedName("pincode")
	val pincode: String? = null,

	@field:SerializedName("used_poString")
	val usedPoString: String? = null,

	@field:SerializedName("coupon_code")
	val couponCode: String? = null,

	@field:SerializedName("address")
	val address: String? = null,

	@field:SerializedName("city")
	val city: String? = null,

	@field:SerializedName("source")
	val source: String? = null,

	@field:SerializedName("is_cod")
	val isCod: String? = null,

	@field:SerializedName("order_date")
	val orderDate: String? = null,

	@field:SerializedName("total_amt")
	val totalAmt: String? = null,

	@field:SerializedName("flat_no")
	val flatNo: String? = null,

	@field:SerializedName("delivery_charges")
	val deliveryCharges: String? = null,

	@field:SerializedName("subtotal")
	val subtotal: String? = null,

	@field:SerializedName("cod_charges")
	val codCharges: String? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("state")
	val state: Any? = null,

	@field:SerializedName("is_paid")
	val isPaid: String? = null,

	@field:SerializedName("updated_date")
	val updatedDate: String? = null,

	@field:SerializedName("cust_id")
	val custId: String? = null,

	@field:SerializedName("payable_amt")
	val payableAmt: String? = null,

	@field:SerializedName("disc_amt")
	val discAmt: String? = null,

	@field:SerializedName("remarks")
	val remarks: Any? = null,

	@field:SerializedName("geolocation")
	val geolocation: Any? = null,

	@field:SerializedName("status")
	val status: String? = null,

	@field:SerializedName("order_items")
	val orderItems: String? = null
)
