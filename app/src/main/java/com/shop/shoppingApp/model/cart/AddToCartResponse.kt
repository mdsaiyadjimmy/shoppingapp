package com.shop.shoppingApp.model.cart

import com.google.gson.annotations.SerializedName

data class AddToCartResponse(

	@field:SerializedName("msg")
	val msg: String? = null,

	@field:SerializedName("data")
	val data: List<CartDataItem> = mutableListOf(),

	@field:SerializedName("error")
	val error: Boolean = false
)

data class CartDataItem(

	@field:SerializedName("amount")
	val amount: String? = null,

	@field:SerializedName("img")
	val img: String? = null,

	@field:SerializedName("variant_id")
	val variantId: Int? = null,

	@field:SerializedName("size")
	val size: String? = null,

	@field:SerializedName("price")
	val price: String? = null,

	@field:SerializedName("product_id")
	val productId: Int? = null,

	@field:SerializedName("qty")
	val qty: Int? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("created_date")
	val createdDate: String? = null,

	@field:SerializedName("cust_id")
	val custId: Int? = null,

	@field:SerializedName("product_name")
	val productName: String? = null
)
