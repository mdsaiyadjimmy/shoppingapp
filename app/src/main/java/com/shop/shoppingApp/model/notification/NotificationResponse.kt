package com.shop.shoppingApp.model.notification

import com.google.gson.annotations.SerializedName

data class NotificationResponse(

	@field:SerializedName("msg")
	val msg: String? = null,

	@field:SerializedName("data")
	val data: List<NotifItem> = mutableListOf(),

	@field:SerializedName("error")
	val error: Boolean = false
)

data class NotifItem(

	@field:SerializedName("title")
	val title: String? = null,
	@field:SerializedName("img")
	val img: String? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("created_date")
	val createdDate: String? = null,


)
