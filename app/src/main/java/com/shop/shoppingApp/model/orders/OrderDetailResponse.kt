package com.shop.shoppingApp.model.orders
import com.google.gson.annotations.SerializedName

data class OrderDetailResponse(
    @SerializedName("data")
    val `data`: Data?,
    @SerializedName("error")
    val error: Boolean?,
    @SerializedName("msg")
    val msg: String?
)

data class Data(
    @SerializedName("address")
    val address: String?,
    @SerializedName("city")
    val city: String?,
    @SerializedName("cod_charges")
    val codCharges: String?,
    @SerializedName("coupon_code")
    val couponCode: String?,
    @SerializedName("cust_id")
    val custId: Int?,
    @SerializedName("delivery_charges")
    val deliveryCharges: String?,
    @SerializedName("disc_amt")
    val discAmt: String?,
    @SerializedName("flat_no")
    val flatNo: String?,
    @SerializedName("geolocation")
    val geolocation: Any?,
    @SerializedName("id")
    val id: Int?,
    @SerializedName("is_cod")
    val isCod: Int?,
    @SerializedName("is_paid")
    val isPaid: Int?,
    @SerializedName("order_date")
    val orderDate: String?,
    @SerializedName("order_items")
    val orderItems: List<OrderItem> = mutableListOf(),
    @SerializedName("payable_amt")
    val payableAmt: String?,
    @SerializedName("pincode")
    val pincode: String?,
    @SerializedName("source")
    val source: String?,
    @SerializedName("state")
    val state: Any?,
    @SerializedName("status")
    val status: String?,
    @SerializedName("subtotal")
    val subtotal: String?,
    @SerializedName("total_amt")
    val totalAmt: String?,
    @SerializedName("updated_date")
    val updatedDate: String?,
    @SerializedName("used_point")
    val usedPoint: String?
)

data class OrderItem(
    @SerializedName("id")
    val id: Int?,
    @SerializedName("img")
    val img: String?,
    @SerializedName("price")
    val price: String?,
    @SerializedName("product_name")
    val productName: String?,
    @SerializedName("qty")
    val qty: Int=0,
    @SerializedName("size")
    val size: String?,
    @SerializedName("weight")
    val weight: String?
)
