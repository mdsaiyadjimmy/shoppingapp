package com.shop.shoppingApp.model.cart

import com.google.gson.annotations.SerializedName

data class CartTotalResponse(

	@field:SerializedName("msg")
	val msg: String? = null,

	@field:SerializedName("data")
	val data: Data? = null,

	@field:SerializedName("error")
	val error: Boolean = false
)

data class Data(

	@field:SerializedName("next_order_no")
	val nextOrderNo : String? = null,

	@field:SerializedName("delivery_charges")
	val deliveryCharges: String? = null,

	@field:SerializedName("net_payable")
	val netPayable: String? = null,
	@field:SerializedName("total_savings")
	val totalSavings : String? = null,

	@field:SerializedName("sub_total")
	val subTotal: String? = null,

	@field:SerializedName("used_wallet")
	val usedWallet: String? = null,

	@field:SerializedName("discount")
	val discount: String? = null,

	@field:SerializedName("order_total")
	val orderTotal: String? = null
)
