package com.shop.shoppingApp.model.wallet

import com.google.gson.annotations.SerializedName

data class WalletResponse(

	@field:SerializedName("msg")
	val msg: String? = null,

	@field:SerializedName("data")
	val data: List<WalletItem> = mutableListOf(),

	@field:SerializedName("error")
	val error: Boolean = false
)

data class WalletItem(

	@field:SerializedName("pointvalue")
	val pointvalue: String? = null,

	@field:SerializedName("refrence_id")
	val refrenceId: Int? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("created_date")
	val createdDate: String? = null,

	@field:SerializedName("cust_id")
	val custId: Int? = null
)
