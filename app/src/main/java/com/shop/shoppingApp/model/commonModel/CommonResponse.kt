package com.shop.shoppingApp.model.commonModel

import com.google.gson.annotations.SerializedName

data class CommonResponse(
    @field:SerializedName("error")
    val error: Boolean = false,
    @field:SerializedName("msg")
    val message: String? = ""
)
