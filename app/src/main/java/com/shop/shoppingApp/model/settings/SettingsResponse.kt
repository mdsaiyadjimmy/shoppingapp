package com.shop.shoppingApp.model.settings

import com.google.gson.annotations.SerializedName

data class SettingsResponse(

	@field:SerializedName("msg")
	val msg: String? = null,

	@field:SerializedName("data")
	val data: Data? = null,

	@field:SerializedName("error")
	val error: Boolean? = null
)

data class Data(

	@field:SerializedName("menu_link")
	val menuLink: String? = null,

	@field:SerializedName("minimum_order")
	val minimumOrder: String? = null,

	@field:SerializedName("delivery_charges")
	val deliveryCharges: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("updated_date")
	val updatedDate: String? = null
)
