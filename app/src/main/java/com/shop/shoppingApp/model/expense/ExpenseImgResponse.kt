package com.shop.shoppingApp.model.expense

import com.google.gson.annotations.SerializedName

data class ExpenseImgResponse(

    @field:SerializedName("msg")
    val msg: String? = null,

    @field:SerializedName("data")
    val data: Data? = null,

    @field:SerializedName("error")
    val error: Boolean? = null
)

data class Data(

    @field:SerializedName("imgs")
    val imgs: List<ImgsItem> = mutableListOf(),

    @field:SerializedName("base_uri")
    val baseUri: String? = null
)

data class ImgsItem(

    @field:SerializedName("img_url")
    val imgUrl: String? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("expense_id")
    val expenseId: Int? = null
)
