package com.shop.shoppingApp.model.productHomepage

import com.google.gson.annotations.SerializedName

data class HomepageResponse(

	@field:SerializedName("msg")
	val msg: String? = null,

	@field:SerializedName("data")
	val data: Data? = null,

	@field:SerializedName("error")
	val error: Boolean? = null
)

data class TopProductsItem(

	@field:SerializedName("img")
	val img: String? = null,

	@field:SerializedName("is_active")
	val isActive: Int? = null,

	@field:SerializedName("sub_category")
	val subCategory: Any? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("hsn_code")
	val hsnCode: String? = null,

	@field:SerializedName("is_discount")
	val isDiscount: Int? = null,

	@field:SerializedName("origin_country")
	val originCountry: String? = null,

	@field:SerializedName("manufacturer")
	val manufacturer: String? = null,

	@field:SerializedName("variant_id")
	val variantId: String? = null,

	@field:SerializedName("vegeterian_type")
	val vegeterianType: Any? = null,

	@field:SerializedName("sizes")
	val sizes: String? = null,

	@field:SerializedName("sps")
	val sps: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("updated_date")
	val updatedDate: String? = null,

	@field:SerializedName("created_date")
	val createdDate: String? = null,

	@field:SerializedName("mrps")
	val mrps: String? = null,

	@field:SerializedName("category")
	val category: String? = null,

	@field:SerializedName("brand")
	val brand: String? = null,

	@field:SerializedName("is_featured")
	val isFeatured: Int? = null,
	var pos: Int = 0
)

data class Data(

	@field:SerializedName("top_products")
	val topProducts: List<TopProductsItem> = mutableListOf(),

	@field:SerializedName("categories")
	val categories: List<CategoriesItem> = mutableListOf(),

	@field:SerializedName("banners")
	val banners: List<BannersItem> = mutableListOf(),

	@field:SerializedName("cart_count")
	val cartCount: Int = 0
)

data class CategoriesItem(

	@field:SerializedName("img")
	val img: String? = null,

	@field:SerializedName("name")
	val name: String? = null
)

data class BannersItem(

	@field:SerializedName("is_active")
	val isActive: Int? = null,

	@field:SerializedName("section")
	val section: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("created_date")
	val createdDate: String? = null,

	@field:SerializedName("url")
	val url: String? = null
)
