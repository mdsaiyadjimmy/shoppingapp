package com.shop.shoppingApp.model.addressModel

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class AddressResponse(

	@field:SerializedName("data")
	val data: List<DataItem> = mutableListOf(),

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class DataItem(

		@field:SerializedName("pincode")
		val pincode: String? = null,

		@field:SerializedName("appartment")
		val appartment: String? = null,

		@field:SerializedName("address")
		val address: String? = null,

		@field:SerializedName("address_type")
		val addressType: String? = null,

		@field:SerializedName("lng")
		val lng: String? = null,

		@field:SerializedName("city")
		val city: String? = null,

		@field:SerializedName("last_name")
		val lastName: String? = null,

		@field:SerializedName("user_id")
		val userId: String? = null,

		@field:SerializedName("contact_no")
		val contactNo: String? = null,

		@field:SerializedName("street")
		val street: String? = null,

		@field:SerializedName("house_no")
		val houseNo: String? = null,

		@field:SerializedName("id")
		val id: String? = null,

		@field:SerializedName("state")
		val state: String? = null,

		@field:SerializedName("landmark")
		val landmark: String? = null,

		@field:SerializedName("first_name")
		val firstName: String? = null,

		@field:SerializedName("lat")
		val lat: String? = null
): Parcelable {
	constructor(parcel: Parcel) : this(
			parcel.readString(),
			parcel.readString(),
			parcel.readString(),
			parcel.readString(),
			parcel.readString(),
			parcel.readString(),
			parcel.readString(),
			parcel.readString(),
			parcel.readString(),
			parcel.readString(),
			parcel.readString(),
			parcel.readString(),
			parcel.readString(),
			parcel.readString(),
			parcel.readString(),
			parcel.readString()) {
	}

	override fun writeToParcel(parcel: Parcel, flags: Int) {
		parcel.writeString(pincode)
		parcel.writeString(appartment)
		parcel.writeString(address)
		parcel.writeString(addressType)
		parcel.writeString(lng)
		parcel.writeString(city)
		parcel.writeString(lastName)
		parcel.writeString(userId)
		parcel.writeString(contactNo)
		parcel.writeString(street)
		parcel.writeString(houseNo)
		parcel.writeString(id)
		parcel.writeString(state)
		parcel.writeString(landmark)
		parcel.writeString(firstName)
		parcel.writeString(lat)
	}

	override fun describeContents(): Int {
		return 0
	}

	companion object CREATOR : Parcelable.Creator<DataItem> {
		override fun createFromParcel(parcel: Parcel): DataItem {
			return DataItem(parcel)
		}

		override fun newArray(size: Int): Array<DataItem?> {
			return arrayOfNulls(size)
		}
	}
}