package com.shop.shoppingApp.model.orders

import com.google.gson.annotations.SerializedName


data class OrdersResponse(
    @SerializedName("data")
    val `data`: List<OrderItems> = mutableListOf(),
    @SerializedName("error")
    val error: Boolean = false,
    @SerializedName("msg")
    val msg: String?
)

data class OrderItems(
    @SerializedName("address")
    val address: String?,
    @SerializedName("city")
    val city: String?,
    @SerializedName("cod_charges")
    val codCharges: String?,
    @SerializedName("coupon_code")
    val couponCode: String?,
    @SerializedName("cust_id")
    val custId: Int?,
    @SerializedName("delivery_charges")
    val deliveryCharges: String?,
    @SerializedName("disc_amt")
    val discAmt: String?,
    @SerializedName("flat_no")
    val flatNo: String?,
    @SerializedName("geolocation")
    val geolocation: String?,
    @SerializedName("id")
    val id: Int?,
    @SerializedName("is_cod")
    val isCod: Int?,
    @SerializedName("is_paid")
    val isPaid: Int?,
    @SerializedName("order_date")
    val orderDate: String?,
    @SerializedName("order_items")
    val orderItems: String?,
    @SerializedName("payable_amt")
    val payableAmt: String?,
    @SerializedName("pincode")
    val pincode: String?,
    @SerializedName("source")
    val source: String?,
    @SerializedName("state")
    val state: String?,
    @SerializedName("status")
    var status: String?,
    @SerializedName("subtotal")
    val subtotal: String?,
    @SerializedName("total_amt")
    val totalAmt: String?,
    @SerializedName("updated_date")
    val updatedDate: String?,
    @SerializedName("used_point")
    val usedPoint: String?
)