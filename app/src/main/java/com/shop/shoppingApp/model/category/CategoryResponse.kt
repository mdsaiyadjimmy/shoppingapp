package com.shop.shoppingApp.model.category

import com.google.gson.annotations.SerializedName
import com.shop.shoppingApp.model.productHomepage.CategoriesItem

data class CategoryResponse(

	@field:SerializedName("msg")
	val msg: String? = null,

	@field:SerializedName("data")
	val data: List<CategoriesItem> = mutableListOf(),

	@field:SerializedName("error")
	val error: Boolean = false
)


