package com.shop.shoppingApp.model.expense

import com.google.gson.annotations.SerializedName

data class ExpenseResponse(

	@field:SerializedName("msg")
	val msg: String? = null,

	@field:SerializedName("data")
	val data: List<ExpemseItem> = mutableListOf(),

	@field:SerializedName("error")
	val error: Boolean? = null
)

data class ExpemseItem(

	@field:SerializedName("amount")
	val amount: String? = null,

	@field:SerializedName("img")
	val img: String? = null,

	@field:SerializedName("comments")
	val comments: String? = null,

	@field:SerializedName("employee_id")
	val employeeId: String? = null,

	@field:SerializedName("exp_date")
	val expDate: String? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("title")
	val title: String? = null,

	@field:SerializedName("category")
	val category: String? = null
)
