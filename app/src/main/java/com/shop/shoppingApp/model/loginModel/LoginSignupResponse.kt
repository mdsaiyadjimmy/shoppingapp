package com.shop.shoppingApp.model.loginModel

import com.google.gson.annotations.SerializedName


data class LoginSignupResponse(

    @SerializedName("data")
    val `data`: Data?,

    @SerializedName("error")
    val error: Boolean?,

    @SerializedName("msg")
    val msg: String?

)
data class Data(
    @SerializedName("address")
    val address: String?,
    @SerializedName("city")
    val city: String?,
    @SerializedName("created_date")
    val createdDate: String?,
    @SerializedName("device_id")
    val deviceId: String?,
    @SerializedName("email")
    val email: String?,
    @SerializedName("flat_no")
    val flatNo: String?,
    @SerializedName("geolocation")
    val geolocation: String?,
    @SerializedName("id")
    val id: Int?,
    @SerializedName("is_active")
    val isActive: Int?,
    @SerializedName("is_verified")
    val isVerified: Int?,
    @SerializedName("last_ip")
    val lastIp: String?,
    @SerializedName("last_login")
    val lastLogin: String?,
    @SerializedName("mobile")
    val mobile: String="",
    @SerializedName("name")
    val name: String?,
    @SerializedName("otp_code")
    val otpCode: String?,
    @SerializedName("parent_id")
    val parentId: String?,
    @SerializedName("password")
    val password: String?,
    @SerializedName("postcode")
    val postcode: String?,
    @SerializedName("profile_pic")
    val profilePic: String?,
    @SerializedName("referral_code")
    val referralCode: String?,
    @SerializedName("remarks")
    val remarks: String?,
    @SerializedName("token")
    val token: String?,
    @SerializedName("wallet")
    val wallet: String?
)

