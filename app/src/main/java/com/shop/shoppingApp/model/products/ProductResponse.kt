package com.shop.shoppingApp.model.products

import com.google.gson.annotations.SerializedName
import com.shop.shoppingApp.model.productHomepage.TopProductsItem

data class ProductResponse(

	@field:SerializedName("msg")
	val msg: String? = null,

	@field:SerializedName("data")
	val data: List<TopProductsItem> = mutableListOf(),

	@field:SerializedName("error")
	val error: Boolean = false
)

