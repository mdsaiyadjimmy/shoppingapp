package com.shop.shoppingApp.util


import android.content.Context
import android.location.Location
import android.location.LocationListener
import android.os.Bundle
import android.os.Looper
import android.util.Log
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.*


class FusedLoc(val c: Context) : GoogleApiClient.ConnectionCallbacks,
    GoogleApiClient.OnConnectionFailedListener {

    private lateinit var listener: LocationListener
    private lateinit var googleApiClient: GoogleApiClient
    private lateinit var locationRequest: LocationRequest
    private lateinit var mLocationCallback: LocationCallback
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private var UPDATE_INTERVAL: Long = 2000
    private var FASTEST_INTERVAL: Long = 2000
    private var loc: Location? = null
    fun connectClient() {
        googleApiClient = GoogleApiClient.Builder(c)
            .addApi(LocationServices.API)
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this).build()
        googleApiClient.connect()
    }

    fun getClient(): GoogleApiClient? {
        return GoogleApiClient.Builder(c)
            .addApi(LocationServices.API)
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this).build()

    }

    fun getLastLoc(): Location {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(c)
        fusedLocationClient.lastLocation.addOnSuccessListener { location: Location? ->
            location?.let { it: Location ->
                // Logic to handle location object
                Log.d("TAG", "getlast loc here: ${it.latitude}")

            } ?: kotlin.run {

            }
        }
        return LocationServices.FusedLocationApi.getLastLocation(googleApiClient)

    }

    fun getCurrentLoc(myinterface: MyInterface): Location? {

        return startLocationUpdates(myinterface)
    }

    fun isConnected(): Boolean {
        return googleApiClient.isConnected
    }

    fun disConnectClient() {
        if (googleApiClient.isConnected) {
            googleApiClient.connect()
        }
    }

    override fun onConnectionFailed(p0: ConnectionResult) {
        Log.d("TAG", "onConnectionFailed")

    }

    override fun onConnected(p0: Bundle?) {

    }

    override fun onConnectionSuspended(p0: Int) {
        Log.d("TAG", "onConnectionSuspended")
    }

    private fun startLocationUpdates(myinterface: MyInterface): Location? {
        listener = LocationListener {
            Log.d("TAG", "location" + it.latitude + it.longitude)
            loc = it
            myinterface.getloc(it)
            googleApiClient.disconnect()
        }

        mLocationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult) {
                //super.onLocationResult(locationResult)
                if (locationResult.locations.isNullOrEmpty()) {
                    loc = locationResult.lastLocation
                    Log.d("TAG", "last new loc here: ${locationResult.lastLocation.latitude}")
                    myinterface.getloc(locationResult.lastLocation)
                    fusedLocationClient.removeLocationUpdates(mLocationCallback)
                } else {
                    loc = Location(locationResult.locations[0])
                    myinterface.getloc(Location(locationResult.locations[0]))
                    Log.d("TAG", "new loc here: ${locationResult.lastLocation.latitude}")
                    fusedLocationClient.removeLocationUpdates(mLocationCallback)
                }

            }
        }
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(c)

        getLocationRequest()?.let {
            Looper.myLooper()?.let { it1 ->
                fusedLocationClient.requestLocationUpdates(
                    it,
                    mLocationCallback,
                    it1
                )
            }
        }
       /* LocationServices.FusedLocationApi.requestLocationUpdates(
            googleApiClient,
            getLocationRequest(),
            listener
        )*/
        return loc
    }

    fun getLocationRequest(): LocationRequest? {
        locationRequest = LocationRequest()
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest.interval = UPDATE_INTERVAL
        locationRequest.fastestInterval = FASTEST_INTERVAL
        return locationRequest
    }


}
