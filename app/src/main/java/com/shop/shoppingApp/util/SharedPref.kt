package com.shop.shoppingApp.util
import android.content.Context
import android.content.SharedPreferences
import com.shop.shoppingApp.ShoppingApp.Companion.appCtx

object SharedPref {

    private val USER_PREFS = "USER"
    private val USER_ID = "ID"
    private val USER_NAME = "NMAE"
    private val USER_EMAIL = "EMAIL"
    private val USER_MOB = "MOB"
    private val USER_PIC = "PIC"
    private val USER_TOTAL_BAL = "BALANCE"
    private val TOKEN = "TOKEN"
    private val USER_TOTAL_LOST = "LOST"
    private val USER_TOTAL_PLAYED = "PLAYED"

    private val ADDRESS_ADDRESS = "ADDRESS_ADDRESS"
    private val ADDRESS_ZIPCODE = "ADDRESS_ZIPCODE"
    private val ADDRESS_CITY = "ADDRESS_CITY"
    private val ADDRESS_STATE = "ADDRESS_STATE"
    private val ADDRESS_HOUSE_NO = "ADDRESS_HOUSE_NO"
    private val HOMEDATA = "HOME_DATA"
    private val REFER_URL = "REFER_URL"
    private val SOUND = "SOUND"
    private val VIBRATE = "VIBRATE"
    private val TTS = "TTS"


    var sharedPreferences: SharedPreferences = appCtx.getSharedPreferences(USER_PREFS, Context.MODE_PRIVATE)
    var editor: SharedPreferences.Editor = sharedPreferences.edit()
    fun getSEARCHHISTORY(): HashSet<String> {

        return sharedPreferences.getStringSet("searchlist", HashSet<String>()) as HashSet<String>
    }

    fun setSEARCHHISTORY(searchlist: String) {
        val mylist=getSEARCHHISTORY().toMutableList()
        if (!mylist.contains(searchlist)) {
            mylist.add(searchlist)
        }
        //myList.toMutableSet()
        editor.putStringSet("searchlist", mylist.toMutableSet()).commit()
    }

    fun setSEARCHHISTORYList(searchlist: List<String>) {

        editor.putStringSet("searchlist", searchlist.toMutableSet()).commit()
    }

    fun getSpecificHISTORY(): HashSet<String> {

        return sharedPreferences.getStringSet("specific", HashSet<String>()) as HashSet<String>
    }

    fun setSpecificHISTORY(searchlist: String) {
        val mylist=getSEARCHHISTORY().toMutableList()
        if (!mylist.contains(searchlist)) {
            mylist.add(searchlist)
        }
        //myList.toMutableSet()
        editor.putStringSet("specific", mylist.toMutableSet()).commit()
    }

    fun setSpecificHISTORYList(searchlist: List<String?>) {

        editor.putStringSet("specific", searchlist.toMutableSet()).commit()
    }
    fun getUSER_ID(): String? {
        return sharedPreferences.getString(USER_ID, "")
    }

    fun setUSER_ID(user_id: String) {
        editor.putString(USER_ID, user_id).commit()
    }

    fun getUSER_PIC(): String? {
        return sharedPreferences.getString(USER_PIC, "")
    }

    fun setUSER_PIC(user_pic: String) {
        editor.putString(USER_PIC, user_pic).commit()
    }

    fun getUSER_NAME(): String? {
        return sharedPreferences.getString(USER_NAME, "")
    }

    fun setUSER_NAME(user_name: String) {
        editor.putString(USER_NAME, user_name).commit()
    }
    fun getUSERTOKEN(): String {
        return sharedPreferences.getString("TYPE", "").toString()
    }

    fun setUSERTOKEN(user_name: String) {
        editor.putString("TYPE", user_name).commit()
    }

    fun getUSER_EMAIL(): String? {
        return sharedPreferences.getString(USER_EMAIL, "")
    }

    fun setUSER_EMAIL(user_email: String) {
        editor.putString(USER_EMAIL, user_email).commit()
    }

    fun getUSER_MOB(): String? {
        return sharedPreferences.getString(USER_MOB, "")
    }

    fun setUSER_MOB(user_mob: String) {
        editor.putString(USER_MOB, user_mob).commit()
    }
    fun getUSER_WHATSAPP(): String? {
        return sharedPreferences.getString("WHATSAPP", "")
    }

    fun setUSER_WHATSAPP(user_mob: String) {
        editor.putString("WHATSAPP", user_mob).commit()
    }

    fun getUSER_TOTAL_BAL(): String? {
        return sharedPreferences.getString(USER_TOTAL_BAL, "")
    }

    fun setUSER_TOTAL_BAL(user_bal: String) {
        editor.putString(USER_TOTAL_BAL, user_bal).commit()
    }

    fun getTOKEN(): String? {
        return sharedPreferences.getString(TOKEN, "")
    }

    fun setTOKEN(token: String) {
        editor.putString(TOKEN, token).commit()
    }


    fun getDriverToken(): String? {
        return sharedPreferences.getString(USER_TOTAL_LOST, "")
    }

    fun setDriverToken(token: String) {
        editor.putString(USER_TOTAL_LOST, token).commit()
    }
    fun getADDRESS_LAT(): String? {
        return sharedPreferences.getString("LAT", "")
    }

    fun setADDRESS_LAT(lat: String) {
        editor.putString("LAT", lat).commit()
    }
    fun getADDRESS_LNG(): String? {
        return sharedPreferences.getString("LNG", "")
    }

    fun setADDRESS_LNG(lng: String) {
        editor.putString("LNG", lng).commit()
    }
    fun getADDRESS_LANDMARK(): String? {
        return sharedPreferences.getString("LND", "")
    }

    fun setADDRESS_LANDMARK(lng: String) {
        editor.putString("LND", lng).commit()
    }
    fun getHelp(): String? {
        return sharedPreferences.getString("HELP", "")
    }

    fun setHelp(help: String) {
        editor.putString("HELP", help).commit()
    }

    fun getADDRESS_ADDRESS(): String? {
        return sharedPreferences.getString(ADDRESS_ADDRESS, "")
    }

    fun setADDRESS_ADDRESS(address_address: String) {
        editor.putString(ADDRESS_ADDRESS, address_address).commit()
    }

    fun getADDRESS_HOUSE(): String? {
        return sharedPreferences.getString("HOUSE", "")
    }

    fun setADDRESS_HOUSE(address_address: String) {
        editor.putString("HOUSE", address_address).commit()
    }

    fun getADDRESS_ZIPCODE(): String? {
        return sharedPreferences.getString(ADDRESS_ZIPCODE, "")
    }

    fun setADDRESS_ZIPCODE(address_zipcode: String) {
        editor.putString(ADDRESS_ZIPCODE, address_zipcode).commit()
    }

    fun getADDRESS_CITY(): String? {
        return sharedPreferences.getString(ADDRESS_CITY, "")
    }

    fun setADDRESS_CITY(address_city: String) {
        editor.putString(ADDRESS_CITY, address_city).commit()
    }

    fun getADDRESS_STATE(): String? {
        return sharedPreferences.getString(ADDRESS_STATE, "")
    }

    fun setADDRESS_STATE(address_city: String) {
        editor.putString(ADDRESS_STATE, address_city).commit()
    }
    fun get_STATE(): String? {
        return sharedPreferences.getString("SEARCH_STATE", "")
    }

    fun set_STATE(address_city: String) {
        editor.putString("SEARCH_STATE", address_city).commit()
    }
    fun getADDRESS_COUNTRY(): String? {
        return sharedPreferences.getString("COUNTRY", "")
    }

    fun setADDRESS_COUNTRY(address_city: String) {
        editor.putString("COUNTRY", address_city).commit()
    }

    fun getHomeData(): String? {
        return sharedPreferences.getString(HOMEDATA, "")
    }

    fun setProjectList(data: String) {
        editor.putString("ProList", data).commit()
    }

    fun getProjectList(): String? {
        return sharedPreferences.getString("ProList", "")
    }

    fun setHomedata(data: String) {
        editor.putString(HOMEDATA, data).commit()
    }

    fun getCARTCNT(): String? {
        return sharedPreferences.getString(REFER_URL, "")
    }

    fun setCARTCNT(refer_url: String) {
        editor.putString(REFER_URL, refer_url).commit()
    }

    fun getONLINESTATUS(): Boolean? {
        return sharedPreferences.getBoolean("STATUS", false)
    }

    fun setONLINESTATUS(status: Boolean) {
        editor.putBoolean("STATUS", status).commit()
    }

    fun getVIBRATE(): String? {
        return sharedPreferences.getString(VIBRATE, "")
    }

    fun setVIBRATE(vibrate: String) {
        editor.putString(VIBRATE, vibrate).commit()
    }

    fun getTTS(): Boolean? {
        return sharedPreferences.getBoolean(TTS,true)
    }

    fun setTTS(tts: Boolean) {
        editor.putBoolean(TTS, tts).commit()
    }

    fun clearSession() {
        editor.clear().commit()
    }


    fun getSKIP(): Boolean {
        return sharedPreferences.getBoolean("SKIP", false)
    }
    fun setSKIP(skip: Boolean) {
        editor.putBoolean("SKIP", skip).commit()
    }

    fun setDEL_ADDRESS_LNG(lng: String) {
        editor.putString("DLNG", lng).commit()

    }

    fun setDEL_ADDRESS_LAT(lat: String) {
        editor.putString("DLAT", lat).commit()
    }

    fun getDEL_ADDRESS_TYPE(): String? {
        return sharedPreferences.getString("DTYPE", "")
    }

    fun setDEL_ADDRESS_TYPE(lat: String) {
        editor.putString("DTYPE", lat).commit()
    }

    fun getDEL_ADDRESS_ID(): String? {
        return sharedPreferences.getString("DID", "")
    }

    fun setDEL_ADDRESS_ID(lat: String) {
        editor.putString("DID", lat).commit()
    }

    fun getDEL_ADDRESS_NAME(): String? {
        return sharedPreferences.getString("DNAME", "")
    }

    fun setDEL_ADDRESS_NAME(lat: String) {
        editor.putString("DNAME", lat).commit()
    }


}