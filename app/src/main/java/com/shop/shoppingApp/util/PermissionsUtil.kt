package com.shop.shoppingApp.util

import android.Manifest
import android.app.Activity
import android.content.pm.PackageManager
import android.util.Log
import androidx.core.app.ActivityCompat

fun Activity.checkLocPermission(): Boolean {
    var check = false

    var permissionsRequired = arrayOf(
        Manifest.permission.ACCESS_FINE_LOCATION,
        Manifest.permission.ACCESS_COARSE_LOCATION
    )

    if (ActivityCompat.checkSelfPermission(this, permissionsRequired[0])
        != PackageManager.PERMISSION_GRANTED
    ) {
        when {
            ActivityCompat.shouldShowRequestPermissionRationale(this, permissionsRequired[0]) -> {
                Log.d("TAG", "if permission")
                ActivityCompat.requestPermissions(
                    this,
                    permissionsRequired,
                    PERMISSION_CALLBACK_CONSTANT
                )

            }
            else -> {
                Log.d("TAG", "else permission")
                ActivityCompat.requestPermissions(
                    this,
                    permissionsRequired,
                    PERMISSION_CALLBACK_CONSTANT
                )
            }
        }
    } else {
        check = true
        Log.d("TAG", "else permission already granted")
    }

    return check
}


fun Activity.checkCamPermission(): Boolean {
    var check = false
    val PERMISSION_CALLBACK_CONSTANT = 100
    var permissionsRequired = arrayOf(
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.CAMERA
    )

    if (ActivityCompat.checkSelfPermission(this, permissionsRequired[1])
        != PackageManager.PERMISSION_GRANTED
    ) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, permissionsRequired[1])) {
            Log.d("TAG", "if permission")
            ActivityCompat.requestPermissions(
                this,
                permissionsRequired,
                PERMISSION_CALLBACK_CONSTANT
            )

        } else {
            Log.d("TAG", "else permission")
            ActivityCompat.requestPermissions(
                this,
                permissionsRequired,
                PERMISSION_CALLBACK_CONSTANT
            )
        }
    } else {
        check = true
        Log.d("TAG", "else permission already granted")
    }

    return check
}
