package com.shop.shoppingApp.util

import android.content.Context
import android.view.animation.AnimationUtils
import com.shop.shoppingApp.R
import com.shop.shoppingApp.ShoppingApp.Companion.appCtx


class Anim {
    val slide_in_left = AnimationUtils.loadAnimation(
        appCtx,
        R.anim.slide_in_left
    )
    val slide_in_right = AnimationUtils.loadAnimation(
        appCtx,
        R.anim.slide_in_right
    )
    val slide_out_right = AnimationUtils.loadAnimation(
        appCtx,
        R.anim.slide_out_right
    )
    val slide_out_left = AnimationUtils.loadAnimation(
        appCtx,
        R.anim.slide_out_left
    )
    val shake = AnimationUtils.loadAnimation(
        appCtx,
        R.anim.shake
    )
    val slide_out_bottom = AnimationUtils.loadAnimation(
        appCtx,
        R.anim.slide_out_bottom
    )
    val slide_out_top = AnimationUtils.loadAnimation(
        appCtx,
        R.anim.slide_out_top
    )
    val animationLogo = AnimationUtils.loadAnimation(
        appCtx,
        R.anim.logo_animation
    )
    val slide_in_bottom = AnimationUtils.loadAnimation(
        appCtx,
        R.anim.slide_in_bottom
    )
    val slide_in_top = AnimationUtils.loadAnimation(
        appCtx,
        R.anim.slide_in_top
    )
    val fadein = AnimationUtils.loadAnimation(
        appCtx,
        R.anim.fade_in
    )
    val fadeout = AnimationUtils.loadAnimation(
        appCtx,
        R.anim.fade_out
    )
    val rotatein = AnimationUtils.loadAnimation(
        appCtx,
        R.anim.rotate_in
    )
    val rotateout = AnimationUtils.loadAnimation(
        appCtx,
        R.anim.rotate_out
    )
    val enter = AnimationUtils.loadAnimation(
        appCtx,
        R.anim.enter
    )
    val exit = AnimationUtils.loadAnimation(
        appCtx,
        R.anim.exit
    )
    val rv_anim = AnimationUtils.loadLayoutAnimation(
        appCtx,
        R.anim.rv_anim
    )
    val rv_disappear_anim = AnimationUtils.loadLayoutAnimation(
        appCtx,
        R.anim.rv_anim_exit
    )
}