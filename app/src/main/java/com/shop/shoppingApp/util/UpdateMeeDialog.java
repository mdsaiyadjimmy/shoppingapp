package com.shop.shoppingApp.util;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.shop.shoppingApp.R;


public class UpdateMeeDialog {

    ActivityManager am;
    TextView rootName;
    Context context;
    Dialog dialog;
    String key1, schoolId;

    public void showDialogAddRoute(final Activity activity, final String packageName) {
        context = activity;
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(R.layout.dialog_update);
        am = (ActivityManager) activity.getSystemService(Context.ACTIVITY_SERVICE);
        TextView ok = dialog.findViewById(R.id.updateOkay);
        ImageView btn_cancelplaystore = dialog.findViewById(R.id.update_dialog_cancel);
        Log.i("package name", packageName);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPref.INSTANCE.clearSession();
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(""));
                context.startActivity(intent);
            }
        });
        btn_cancelplaystore.setOnClickListener(v -> dialog.dismiss());

        dialog.show();
    }
}
