package com.shop.shoppingApp.util

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.*
import android.util.Log
import android.util.Patterns
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.shop.shoppingApp.R
import com.shop.shoppingApp.ShoppingApp.Companion.appCtx
import com.shop.shoppingApp.api.IMGBASE_URL
import com.shop.shoppingApp.api.IMGBASE_URL_PROFILE
import com.shop.shoppingApp.main.activity.MainActivity
import kotlinx.android.synthetic.main.dialog_success.*
import kotlinx.android.synthetic.main.option_dialog.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File
import java.text.DecimalFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import kotlin.random.Random


fun String?.setAmount(): String {
    var df = DecimalFormat("#.##")
    return if (this.isNullOrEmpty()) "₹00"
    else "₹" + df.format(this.toDouble()).toString()
}


fun String?.getAmount(): Double {
    var df = DecimalFormat("#.##")
    return if (this.isNullOrEmpty()) "00".toDouble()
    else df.format(this.toDouble()).toDouble()
}

fun toast(msg: String?) {
    Toast.makeText(
        appCtx, "" + msg,
        Toast.LENGTH_SHORT
    ).show()
}

fun View.hide() {
    this.visibility = View.GONE
}

fun View.show() {
    this.visibility = View.VISIBLE
}

fun View.invisible() {
    this.visibility = View.INVISIBLE
}

fun log(tag: String? = "TAG", msg: String? = "Log here") {
    Log.d(tag, msg.toString())
}

fun Context.startActivity(activity: Activity) {
    this.startActivity(Intent(this, activity::class.java))
}

fun String.capsFirstWord(): String {
    val str = this.lowercase()
    return if (str.isNotEmpty()) str.substring(0, 1).uppercase() + str.substring(1)
    else ""
}

fun String?.getList(sepBy: String = ","): List<String> {
    return try {
        this?.split(sepBy)!!.map { it.trim() }
    } catch (e: Exception) {
        listOf()
    }
}

fun String.capsWord(): String {
    var mystr = ""
    var str1 = this.lowercase()
    if (this.isNotEmpty()) {
        val ar = str1.split(" ")

        ar.forEach {
            if (it.isNotEmpty()) mystr += it.substring(
                0,
                1
            ).uppercase() + it.substring(1) + " "
        }
    }
    return if (this.isNotEmpty()) mystr
    else ""
}

fun String.getFormatedDate(): String {
    val s = ""
    val formatter = SimpleDateFormat("yyyy/MM/dd")
    //2022-02-16 08:30:33
    val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    try {
        val d = sdf.parse(this)
        return formatter.format(d)
    } catch (ex: ParseException) {
        Log.v("TAG", ex.localizedMessage)
    }
    return s
}

fun ImageView.setImgProfile(url: String?) {
    if (url.isNullOrEmpty() || url == "null") {
        Glide.with(appCtx).load(R.drawable.ic_user)
            .circleCrop()
            .into(this)
    } else {
        Glide.with(appCtx).load(IMGBASE_URL_PROFILE + url)
            .error(R.drawable.ic_user)
            .thumbnail(Glide.with(appCtx).load(R.drawable.loader_gif).circleCrop())
            .placeholder(R.drawable.ic_user)
            .circleCrop()
            .into(this)
    }
}


fun ImageView.setImg(url: String?) {
    if (url.isNullOrEmpty()) {
        Glide.with(appCtx).load(R.drawable.ic_image_place_holder)
            .into(this)
    } else {
        Glide.with(appCtx).load(IMGBASE_URL + url).transform(CenterCrop())
            .error(R.drawable.ic_image_place_holder)
            .thumbnail(Glide.with(appCtx).load(R.drawable.loader_gif))
            .placeholder(R.drawable.ic_image_place_holder)
            .into(this)
    }
}

fun ImageView.setImg(url: Int?) {
    Glide.with(appCtx).load(url)
        .into(this)
}

fun ImageView.setImg(url: Uri?, radius: Int = 0) {
    if (url == null) {
        Glide.with(appCtx).load(R.drawable.ic_image_place_holder)
            .into(this)
    } else {
        Glide.with(appCtx).load(url).transform(CenterCrop(), RoundedCorners(radius))
            .error(R.drawable.ic_image_place_holder)
            .thumbnail(Glide.with(appCtx).load(R.drawable.loader_gif))
            .placeholder(R.drawable.ic_image_place_holder)
            .into(this)
    }
}


fun Activity.fulscr() {
    this.window.setFlags(
        WindowManager.LayoutParams.FLAG_FULLSCREEN,
        WindowManager.LayoutParams.FLAG_FULLSCREEN
    )
}

fun Activity?.whiteStatusBar() {
//    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
//        this?.window?.decorView?.systemUiVisibility =
//            View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
//    }
}

fun Activity.noStatusNoNav() {
    this.window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_LAYOUT_STABLE
            or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
            or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
            or View.SYSTEM_UI_FLAG_FULLSCREEN
            or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY)

}

fun Activity?.statusBarTrans() {
    this?.window?.setFlags(
        WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
        WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
    )
}

fun Context?.vibrate() {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        val vib = this?.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
        vib.vibrate(VibrationEffect.createOneShot(50, 1))
    } else {
        val vib = this?.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
        vib.vibrate(50)
    }
}

fun Activity?.clickablefalse() {
    this?.window?.setFlags(
        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
    )
}

fun Activity?.clickabletrue() {
    this?.window?.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
}


fun String?.isValidEmail(): Boolean {
    return this.toString().isNotEmpty() && Patterns.EMAIL_ADDRESS.matcher(this).matches()
}

fun String?.isValidPhone(): Boolean {
    return this.toString()
        .isNotEmpty() && this.toString().length > 9 && Patterns.PHONE.matcher(this).matches()
}

fun EditText?.check(minLength: Int = 0): Boolean {

    return when {
        this?.text.toString().isEmpty() -> {
            this?.error = "Field Required"
            this?.requestFocus()
            true
        }
        this?.text.toString().trim().length < minLength -> {
            this?.error = "Minimum length should be $minLength"
            this?.requestFocus()
            true
        }
        else -> false
    }
}


fun getRandomClr(): ColorDrawable {
    val currentColor =
        Color.argb(
            255, Random.nextInt(256), Random.nextInt(256),
            Random.nextInt(256)
        )
    return ColorDrawable(currentColor)
}

fun getadp(list: List<String>): ArrayAdapter<String> {
    return ArrayAdapter(
        appCtx,
        R.layout.item_spiner,
        list
    )
}

fun View.hideSoftKeyboard() {

    val imm = appCtx.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
    imm?.hideSoftInputFromWindow(this.windowToken, 0)


}

fun Context.optionDialog(option: String, fxn: (isTrue: Boolean) -> Unit) {
    BottomSheetDialog(this).apply {
        setContentView(R.layout.option_dialog)
        dialog_title.text = "Are You sure want to $option ?"
        dialog_yes.text = option
        dialog_yes.setOnClickListener {
            dismiss()
            fxn(true)
        }
        dialog_no.setOnClickListener {
            dismiss()
            fxn(false)
        }
        show()
    }
}

fun Context.successDialog(
    msg: String = "Order placed successfully",
    orderId: String = "00",
    fxn: (isTrue: Boolean) -> Unit
) {
    Dialog(this).apply {
        setContentView(R.layout.dialog_success)
//        window?.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        successImg.setImg(R.drawable.success)
        successTxt.text = msg
        successOrderId.text = "Order Id #$orderId"
        setCancelable(false)
        setCanceledOnTouchOutside(false)
        Handler(Looper.myLooper()!!).postDelayed({
            fxn(true)
        }, 5000)
        show()
    }
}

fun TextView?.setTextClr(clr: Int) {
    this?.setTextColor(ContextCompat.getColor(appCtx, clr))
}

fun View?.setBgClr(clr: Int) {
    this?.setBackgroundColor(ContextCompat.getColor(appCtx, clr))
}

fun String?.getReqBody(): RequestBody {
    log("TAG", this)
    return RequestBody.create(MediaType.parse("text/plain"), this.toString())
}

fun Uri?.getReqBody(str: String = "profile_pic"): MultipartBody.Part? {
    return try {
        var file = File(this?.path)
        MultipartBody.Part.createFormData(
            str, file.name, RequestBody.create(
                MediaType.parse("image/png"), file
            )
        )
    } catch (e: Exception) {
        log("TAG", e.message)
        null
    }
}

fun getpercent(mrp: Double, sprice: Double): Int {
    var per = 0
    if (mrp > sprice) {
        var f = 0.0
        var disc = mrp - sprice
        f = (disc / mrp) * 100
        per = f.toInt()

    }
    return per
}