package com.shop.shoppingApp.util

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.*
import com.google.android.gms.location.places.ui.PlaceAutocomplete
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MapStyleOptions
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.piashsarker.www.easy_utils_lib.Utils

import com.google.android.libraries.places.api.net.FetchPlaceRequest
import com.google.android.libraries.places.api.net.PlacesClient
import com.google.android.libraries.places.widget.Autocomplete
import com.houzzcartuk.model.placeModel.ResponsePlacesList
import com.shop.shoppingApp.main.activity.MainActivity
import kotlinx.android.synthetic.main.activity_my_new_map.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.util.*
import com.shop.shoppingApp.R
import com.shop.shoppingApp.api.RetrofitClient
import kotlinx.android.synthetic.main.item_places_address.view.*

class MyNewMapActivity : AppCompatActivity(), OnMapReadyCallback,
    GoogleApiClient.ConnectionCallbacks,
    GoogleApiClient.OnConnectionFailedListener {
    lateinit var dialogBox: MyDialogBox
    var city = ""
    var address = ""
    var postalCode = ""
    var lat = ""
    var lng = ""
    var cnt = 1
    var type = ""
    var mapFragment: SupportMapFragment? = null
    var map: GoogleMap? = null
    var startLatLng = LatLng(30.700777, 76.868308)
    var PLACE_AUTOCOMPLETE_REQUEST_CODE = 1
    var lat_changeaddress1: Double? = null
    var lng_changeaddress1: Double? = null
    var destination_name = ""
    var pincode = ""
    var state = ""
    var country = ""
    var REQUEST_LOCATION = 199
    lateinit var changelatlng1: LatLng
    private lateinit var googleApiClient: GoogleApiClient
    private lateinit var locationRequest: LocationRequest
    private lateinit var mLocationCallback: LocationCallback
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private var UPDATE_INTERVAL: Long = 2000
    private var FASTEST_INTERVAL: Long = 2000
    private var c = this@MyNewMapActivity
    var mainlist = mutableListOf<ResponsePlacesList.PredictionsItem>()
    lateinit var adpter: PlacesListAdp
    var location = ""
    var key = ""
    var handler = Handler()
    var delay: Long = 1500
    var last_text_edit: Long = 0
    var canSearch = true
    val searchRunable = Runnable {
        if (System.currentTimeMillis() > (last_text_edit + delay - 500)) {
            canSearch = false
            if (key.isNotEmpty()) search(key.trim())

        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fulscr()
        setContentView(R.layout.activity_my_new_map)
        dialogBox = MyDialogBox(c, true)
        type = intent.getStringExtra("type").toString()
        mapFragment = this.supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment?.getMapAsync(this)
        connectClient()
        rv.layoutManager = LinearLayoutManager(c)
        adpter = PlacesListAdp(c, mainlist)
        rv.adapter = adpter

        my_map_my_loc.setOnClickListener {
            getcurrLoc()
        }
        btn_save.setOnClickListener {
            if (type == "1") {
                val intent = Intent()
                intent.putExtra("area", txt_location.text.toString())
                intent.putExtra("lat", lat)
                intent.putExtra("lng", lng)
                intent.putExtra("city", city)
                intent.putExtra("state", state)
                intent.putExtra("country", country)
                intent.putExtra("pin", postalCode)
                intent.putExtra("mapActivity", "1")
                setResult(11, intent)
                finish()
            } else if (type == "2") {
                SharedPref.set_STATE(state)
                intent.putExtra("state", state)
                intent.putExtra("country", country)
                setResult(11, intent)
                finish()
            } else {
                startActivity(Intent(c, MainActivity::class.java).putExtra("main", true))
            }
        }
        clear_text.setOnClickListener { my_map_search_txt.setText("") }

        my_map_search_txt.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                handler.removeCallbacks(searchRunable)

            }

            override fun afterTextChanged(s: Editable?) {
                clear_text.visibility = if (s.toString().isNotEmpty()) {
                    View.VISIBLE
                } else {
                    View.GONE
                }
                key = s.toString()

                if (key.length >= 3) {

                    if (canSearch) {

                        last_text_edit = System.currentTimeMillis()
                        handler.postDelayed(searchRunable, delay)
                    }

                } else if (s.toString().isNullOrEmpty()) {
                    mainlist.clear()
                    adpter.notifyDataSetChanged()
                }
                //
            }
        })
        rv.addOnItemTouchListener(
            RecyclerItemClickListener(this,
                RecyclerItemClickListener.OnItemClickListener { view, position ->
                    if (mainlist.isNotEmpty()) {
                        getPlaces(mainlist[position].placeId.toString())
                        rv.visibility = View.GONE
                    }
                })
        )
    }

    fun getPlaces(id: String) {
        mainlist.clear()
        adpter.notifyDataSetChanged()
        Log.d("TAG", "id here: $id")

        val placeFields: List<Place.Field> =
            listOf(
                Place.Field.ID,
                Place.Field.NAME,
                Place.Field.ADDRESS,
                Place.Field.ADDRESS_COMPONENTS,
                Place.Field.PLUS_CODE,
                Place.Field.LAT_LNG
            )
       // Places.initialize(c, getString(R.string.google_api_key))
        val placesClient: PlacesClient = Places.createClient(c)

        val request: FetchPlaceRequest = FetchPlaceRequest.builder(id, placeFields).build()
        placesClient.fetchPlace(request).addOnSuccessListener { response ->
            val place =
                response.place
            Log.d("TAG", "Place found: " + place.name)
            try {
                map!!.moveCamera(CameraUpdateFactory.newLatLng(place.latLng))
                map!!.animateCamera(CameraUpdateFactory.zoomTo(15f))
            } catch (e: Exception) {
                toast(getString(R.string.something_wrong))
            }

        }.addOnFailureListener { exception ->
            if (exception is ApiException) {
                val apiException: ApiException = exception as ApiException
                val statusCode: Int = apiException.statusCode

                Log.d(
                    "TAG", "Place not found: " + exception.message
                )
            }
        }
    }

    private fun checkPerm() {
        if (checkLocPermission()) {
            if (Utils.isGPSEnable(c)) {
                // dialogBox.show()
                if (googleApiClient.isConnected) getLastLoc()
                else googleApiClient.connect()
            } else {
                Util.checkGps(c)
            }
        }
    }


    private fun search(key: String) {
        if (NetworkUtil.isConnected()) {
            getplace(
                RetrofitClient().getPlacesList(
                    key = "",//getString(R.string.google_api_key),
                    input = key
                )
            )
        }
    }

    fun getplace(call: Call<ResponsePlacesList>) {
        dialogBox.show()
        call.enqueue(object : Callback<ResponsePlacesList> {
            override fun onResponse(
                call: Call<ResponsePlacesList>,
                response: Response<ResponsePlacesList>
            ) {
                try {
                    mainlist.clear()
                    canSearch = true
                    dialogBox.dismiss()
                    val res = response.body()
                    Log.d("TAG", "otp response......." + res?.toString())
                    if (response.isSuccessful && res != null) {
                        //skip 0 for otp dialog,1 for login success,2 for send token
                        if (response.code() == 200 && response.body()?.status.toString() == "OK") {
                            mainlist.addAll(res.predictions)
                            adpter.notifyDataSetChanged()
                            rv.visibility = if (mainlist.isEmpty()) View.GONE
                            else View.VISIBLE
                            if (res.predictions.isNullOrEmpty()) toast( "" + res.status)
                        }
                    } else {
                        toast( getString(R.string.something_wrong))
                    }

                } catch (e: java.lang.Exception) {
                    //  shimmer.visibility = View.GONE
                    toast( getString(R.string.something_wrong))
                    dialogBox.dismiss()
                }

                canSearch = true
            }

            override fun onFailure(call: Call<ResponsePlacesList>, t: Throwable) {
                Log.d("TAG", "" + t.message)
                canSearch = true
                try {
                    dialogBox.dismiss()

                } catch (e: java.lang.Exception) {

                }
                toast( getString(R.string.something_wrong))
            }
        })
    }

    private fun getcurrLoc() {
        if (checkLocPermission()) {
            if (Utils.isGPSEnable(c)) {
                // dialogBox.show()
                if (googleApiClient.isConnected) startLocationUpdates()
                else googleApiClient.connect()
            } else {
                Util.checkGps(c)
            }
        }
    }

    private fun connectClient() {
        googleApiClient = GoogleApiClient.Builder(c)
            .addApi(LocationServices.API)
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this)
            .build()
        googleApiClient.connect()
    }

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        /*map!!.moveCamera(CameraUpdateFactory.newLatLng(startLatLng))
        map!!.animateCamera(CameraUpdateFactory.zoomTo(15f))*/
        //

        if (checkLocPermission()) map?.isMyLocationEnabled = true
        map?.uiSettings?.isIndoorLevelPickerEnabled = true
        map?.uiSettings?.isCompassEnabled = false
        map?.setMapStyle(
            MapStyleOptions.loadRawResourceStyle(
                this, R.raw.style_map_silver
            )
        )
        if (mapFragment?.view != null && mapFragment?.requireView()
                ?.findViewById<View>(Integer.parseInt("1")) != null
        ) {
            val locationButton =
                (mapFragment?.requireView()
                    ?.findViewById<View>(Integer.parseInt("1"))?.parent as View).findViewById<View>(
                    Integer.parseInt("2")
                )
            val layoutParams = locationButton.layoutParams as RelativeLayout.LayoutParams
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0)
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE)
            layoutParams.setMargins(
                0,
                0, //card_search.measuredHeight + resources.getDimension(R.dimen._20sdp).toInt(),
                0,
                resources.getDimension(com.intuit.sdp.R.dimen._50sdp).toInt()
            )
            locationButton.setPadding(
                0,
                0,
                0,
                0
            )
            locationButton.layoutParams = layoutParams
        }

        if (map != null) {
            map!!.setOnCameraIdleListener {
                val midLatLng = map!!.cameraPosition.target
                lat = midLatLng.latitude.toString()
                lng = midLatLng.longitude.toString()
                Log.d("TAG", "move")
                val geocoder = Geocoder(this@MyNewMapActivity, Locale.getDefault())
                var addresses: List<Address>? = null
                if (lat != null) {
                    try {
                        addresses = geocoder.getFromLocation(
                            midLatLng.latitude,
                            midLatLng.longitude,
                            1
                        ) // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                        val address1 =
                            addresses!![0].getAddressLine(0) // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                        city = addresses[0].locality
                        postalCode = addresses[0].postalCode
                        state = addresses[0].adminArea
                        country = addresses[0].countryName
                        address = address1
                        txt_location.text = address1
                        if (!address1.isNullOrEmpty()) {
                            showbtn()
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                        Log.d("TAG", "catch")
                    }
                }
            }
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                location = "1"
                // val place = PlaceAutocomplete.getPlace(this@MyNewMapActivity, data!!)
                val place = Autocomplete.getPlaceFromIntent(data!!)
                Log.i("Place: ", place.name.toString() + " " + place.address)
                destination_name = place.address!!.toString() + ""
                val lati = place.latLng.toString()
                val latlng2 = place.latLng
                val lat1 = latlng2?.latitude
                val lng2 = latlng2?.longitude
                val latlng =
                    lati.split("lat/lng:".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                val latlng1 = latlng[1]
                val latlngArray =
                    latlng1.split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                val lat12 = latlngArray[0]
                val lat13 = latlngArray[1]
                val mGeocoder = Geocoder(c, Locale.getDefault())
                var addressess: List<Address>? = null
                try {
                    addressess = mGeocoder.getFromLocation(lat1!!, lng2!!, 1)
                    if (addressess != null && addressess.size > 0) {
                        city = addressess[0].locality
                    }
                } catch (e: IOException) {
                    e.printStackTrace()
                }

                lat = lat12.replace("\\(".toRegex(), "")
                lng = lat13.replace("\\)".toRegex(), "")
                lat_changeaddress1 = java.lang.Double.valueOf(lat)
                lng_changeaddress1 = java.lang.Double.valueOf(lng)
                changelatlng1 = LatLng(lat_changeaddress1!!, lng_changeaddress1!!)
                val geocoder = Geocoder(this, Locale.getDefault())
                try {
                    map!!.moveCamera(CameraUpdateFactory.newLatLng(changelatlng1))
                } catch (e: Exception) {

                }
                map!!.animateCamera(CameraUpdateFactory.zoomTo(15f))
                try {
                    val addresses = geocoder.getFromLocation(lat1!!, lng2!!, 1)
                    pincode = addresses[0].postalCode
                } catch (e: Exception) {

                }

                txt_location.text = destination_name
            }

        } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {

            val status = PlaceAutocomplete.getStatus(this@MyNewMapActivity, data!!)

        } else if (requestCode == REQUEST_GPS_ENABLE) {

            if (resultCode == Activity.RESULT_OK) {
                checkPerm()
            } else {
                dialogBox.dismiss()
            }
        }
    }


    fun showbtn() {
        if (ll_bottom_sheet.visibility != View.VISIBLE) {
            ll_bottom_sheet.visibility = View.VISIBLE
            ll_bottom_sheet.animation = Anim().slide_in_bottom
        }
    }

    override fun onConnected(p0: Bundle?) {
        checkPerm()

    }

    override fun onConnectionSuspended(p0: Int) {
        toast( "Check Internet Connection")
    }

    override fun onConnectionFailed(p0: ConnectionResult) {
        toast( "Check Internet Connection")
    }

    private fun startLocationUpdates() {
        dialogBox.show()
        mLocationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult) {
                //super.onLocationResult(locationResult)
                if (locationResult.locations.isNullOrEmpty()) {
                    // loc = locationResult.lastLocation
                    Log.d("TAG", "last new loc here: ${locationResult.lastLocation.latitude}")
                    setMap(locationResult.lastLocation)
                    dialogBox.dismiss()
                    fusedLocationClient.removeLocationUpdates(mLocationCallback)
                } else {
                    // loc = Location(locationResult.locations[0])
                    Log.d("TAG", "new loc here: ${locationResult.lastLocation.latitude}")
                    setMap(locationResult.locations[0])
                    dialogBox.dismiss()
                    fusedLocationClient.removeLocationUpdates(mLocationCallback)
                }

            }
        }
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(c)

        Looper.myLooper()?.let {
            getLocationRequest()?.let { it1 ->
                fusedLocationClient.requestLocationUpdates(
                    it1,
                    mLocationCallback,
                    it
                )
            }
        }

    }

    fun getLastLoc() {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(c)
        fusedLocationClient.lastLocation.addOnSuccessListener { location: Location? ->
            location?.let { it: Location ->
                Log.d("TAG", "getlast loc here: ${it.latitude}")
                setMap(it)
            } ?: kotlin.run {
            }
        }


    }

    fun getLocationRequest(): LocationRequest? {
        locationRequest = LocationRequest()
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest.interval = UPDATE_INTERVAL
        locationRequest.fastestInterval = FASTEST_INTERVAL
        return locationRequest
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == 100) {
            Log.d("TAG", "permission.. ${permissions[1]}")
            Log.d("TAG", "permission grant.. ${grantResults[1]}")
            if (grantResults.size > 1 && grantResults[0] > -1) {
                Log.d("TAG", "2")
                checkPerm()
            }

        } else {
            Log.d("TAG", "permission.. $permissions")
        }
    }

    fun setMap(loc: Location) {
        startLatLng = LatLng(loc.latitude, loc.longitude)
        val location = CameraUpdateFactory.newLatLngZoom(startLatLng, 15f)
        if (map != null) map!!.animateCamera(location)

    }


    class PlacesListAdp(
        private val mContext: Context,
        private var mPredictionArray: MutableList<ResponsePlacesList.PredictionsItem>
    ) : RecyclerView.Adapter<PlacesListAdp.MyViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
            val v = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_places_address, parent, false)
            return MyViewHolder(v)
        }

        override fun getItemCount(): Int {
            return mPredictionArray?.size!!
        }

        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
            holder.itemView.tvAddress.text = mPredictionArray?.get(position)?.description
        }

        class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)


    }
}
