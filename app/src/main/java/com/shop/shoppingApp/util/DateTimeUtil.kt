package com.shop.shoppingApp.util

import android.app.DatePickerDialog
import android.util.Log
import android.widget.EditText
import android.widget.TextView
import com.shop.shoppingApp.ShoppingApp
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

fun TextView.getdate() {
    var date = ""
    var calendar = Calendar.getInstance()
    DatePickerDialog(
        ShoppingApp.appCtx, { view, year, month, dayOfMonth ->
            val m = (month + 1)
            date = "$year-$m-$dayOfMonth"
            this.text = date
        }, calendar
            .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
        calendar.get(Calendar.DAY_OF_MONTH)
    ).show()

}

fun String.getdate(): String {
    val s = ""
    val formatter = SimpleDateFormat("dd/MMM/yyyy")
    val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    try {
        val d = sdf.parse(this)
        return formatter.format(d)
    } catch (ex: ParseException) {
        Log.v("TAG", ex.localizedMessage)
    }

    return s
}

fun Long.getDateByTimestamp(): String {
    val s = ""
    val formatter = SimpleDateFormat("dd/MMM/yyyy")
    try {
        return formatter.format(Date(this))
    } catch (ex: ParseException) {
        Log.v("TAG", ex.localizedMessage)
    }

    return s
}
fun String.getFormatedDate(formate: String): String {
    val s = ""
    val formatter = SimpleDateFormat(formate)
    val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    try {
        val d = sdf.parse(this)
        return formatter.format(d)
    } catch (ex: ParseException) {
        Log.v("TAG", ex.localizedMessage)
    }

    return s
}
