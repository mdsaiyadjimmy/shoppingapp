package com.shop.shoppingApp.util

import android.app.Activity
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.view.animation.LayoutAnimationController
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.shop.shoppingApp.R
import kotlinx.android.synthetic.main.dialog_rv.view.*
import kotlinx.android.synthetic.main.item_bottomsheet_dialog.view.*

class BottomUpSheet {

    /**
     * Dummy Data
     */
    private var mDataList = mutableListOf<String>()
    private var searchList = mutableListOf<String>()

    /**
     * Adapter of RecyclerView
     */
    var mAdapter: MyAdapter = MyAdapter(mDataList, this)

    /**
     * LayoutManager for RecyclerView
     */
    lateinit var mLayoutManager: LinearLayoutManager

    /**
     * BottomSheet Object
     */
    private var sheet: BottomSheetDialog? = null

    /**
     * Recycler LayoutAnimation
     */
    lateinit var layoutAnimation: LayoutAnimationController

    /**
     * Listner
     */
    private var mListener: OnOptionSelectedListener? = null

    companion object {

        var mInstance: BottomUpSheet? = null

        fun getSheet(): BottomUpSheet {
            if (mInstance != null) {
                Log.d("TAG", "OLD")
                return mInstance as BottomUpSheet
            } else {
                Log.d("TAG", "New")
                mInstance = BottomUpSheet()
                return mInstance!!
            }
        }
    }

    fun buildBottotmUpSheet(mActivity: Activity, mTitle: String, list: MutableList<String>) {

        this.mDataList.clear()
        this.mDataList.addAll(list)
        this.searchList.clear()
        this.searchList.addAll(list)
        this.mAdapter.notifyDataSetChanged()
        mLayoutManager = LinearLayoutManager(mActivity.applicationContext)
        layoutAnimation = AnimationUtils.loadLayoutAnimation(mActivity, R.anim.layout_bottom_to_up)

        if (sheet != null) {
            sheet?.cancel()
        }

        sheet = BottomSheetDialog(mActivity)

        val sheetView = mActivity.layoutInflater.inflate(R.layout.dialog_rv, null)
        sheet?.setContentView(sheetView)

        sheetView.tv_title.text = mTitle
        sheetView.rv.layoutManager = mLayoutManager
        sheetView.rv.layoutAnimation = layoutAnimation
        sheetView.rv.adapter = mAdapter

       /* sheet_view.rv.addOnItemTouchListener(
            RecyclerItemClickListener(
                mActivity,
                RecyclerItemClickListener.OnItemClickListener { view, position ->

                })
        )*/



    }

    fun show() {
        sheet?.show()
    }
    fun sel(position:Int) {
        mListener?.onOptionSelected(mAdapter, position, mAdapter.selectedItem(position))
        sheet?.dismiss()
        sheet = null
    }

    fun hide() {
        sheet?.dismiss()
    }

    fun setCustomObjectListener(listener: OnOptionSelectedListener) {
        this.mListener = listener
    }

    /**
     * Iinterface for Passing Data
     */
    interface OnOptionSelectedListener {
        fun onOptionSelected(mAdapter: MyAdapter, position: Int, selectedOption: String)
    }


    class MyAdapter(
        private var myDataset: MutableList<String>,
        val bottomUpSheetSearch: BottomUpSheet
    ) :
        RecyclerView.Adapter<MyAdapter.MyViewHolder>() {

        inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var option: TextView

            init {
                option = view.tv_option
            }
        }

        override fun onCreateViewHolder(
            parent: ViewGroup,
            viewType: Int
        ): MyViewHolder {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_bottomsheet_dialog, parent, false)
            return MyViewHolder(view)
        }

        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
            holder.option.text = myDataset[position].capsWord()
            holder.option.isSelected = true
            holder.itemView.setOnClickListener {
                bottomUpSheetSearch.sel(position)
                Log.d("TAG","clicked")
            }
        }

        fun selectedItem(position: Int): String {
            return myDataset[position]
        }

        override fun getItemCount() = myDataset.size


    }
}