package com.shop.shoppingApp.util

import android.Manifest
import android.app.Activity
import android.app.DatePickerDialog
import android.content.*
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.media.ExifInterface
import android.net.Uri
import android.os.Build
import android.os.VibrationEffect
import android.os.Vibrator
import android.provider.MediaStore
import android.util.Base64
import android.util.Log
import android.util.Patterns
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.bumptech.glide.load.resource.bitmap.TransformationUtils.rotateImage
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.LocationSettingsRequest
import com.google.android.gms.location.LocationSettingsStatusCodes
import com.shop.shoppingApp.R
import com.shop.shoppingApp.ShoppingApp.Companion.appCtx
import java.net.URL
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import javax.net.ssl.HttpsURLConnection
import kotlin.random.Random

object Util {

    fun checkGps(c: Activity) {
        val client = FusedLoc(c)
        var googleApiClient = client.getClient()
        googleApiClient?.connect()
        val locationRequest = LocationRequest.create()
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest.interval = 1000
        val builder = LocationSettingsRequest.Builder().addLocationRequest(locationRequest)
        builder.setAlwaysShow(true)
        val result =
            googleApiClient?.let { LocationServices.SettingsApi.checkLocationSettings(it, builder.build()) }
        result?.setResultCallback { result ->
            val status = result.status
            when (status.statusCode) {
                LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> try {

                    status.startResolutionForResult(c, REQUEST_GPS_ENABLE)
                } catch (e: IntentSender.SendIntentException) {
                    Toast.makeText(c, "Errror", Toast.LENGTH_SHORT).show()
                }

            }
        }
    }


    fun getpercent(sprice: Int, mrp: Int): Int {
        var per = 0
        if (mrp > sprice) {
            var f = 0.0
            var disc = mrp - sprice
            f = (disc.toDouble() / mrp.toDouble()) * 100
            // per = (disc/mrp) * 100

            per = f.toInt()
            Log.d("TAG", "dppp: " + f)
            Log.d("TAG", "dppp2: " + per)
        }
        return per
    }

    fun getamnt(per: Int, mrp: Int): Double {
        val prc = mrp.toDouble() * (per.toDouble() / 100)
        Log.d("TAG", "total here :" + prc)
        return mrp - prc
    }


    fun getFacebookProfilePicture(userID: String): Bitmap? {
        var fb_img: Bitmap? = null
        try {
            val fb_url =
                URL("http://graph.facebook.com/$userID/picture?type=small")//small | noraml | large
            val conn1 = fb_url.openConnection() as HttpsURLConnection
            HttpsURLConnection.setFollowRedirects(true)
            conn1.instanceFollowRedirects = true
            fb_img = BitmapFactory.decodeStream(conn1.inputStream)
            return fb_img
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return fb_img
    }




    fun my_frag(sf: FragmentManager, frame: Int, frag: Fragment) {
        val transact = sf.beginTransaction()
         transact.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
        transact.replace(frame, frag).addToBackStack("abc").commit()
    }



    fun hashkey() {
        try {
            val info = appCtx.packageManager.getPackageInfo(
                "com.houzzcartuk",
                PackageManager.GET_SIGNATURES
            )
           // jofzZ8XN3GGKuYawlWrN2xVJl8E=
            for (signature in info.signatures) {
                val md = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                Log.d("KeyHash1:", Base64.encodeToString(md.digest(), Base64.DEFAULT))
            }
        } catch (e: PackageManager.NameNotFoundException) {
        } catch (e: NoSuchAlgorithmException) {
        }
    }






}


